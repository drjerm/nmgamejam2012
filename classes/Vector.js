function Vector(_x,_y)
{
	this.x = _x;
	this.y = _y;
	
	/**
	 * Returns true if this Vector equals the parameter Vector.
	 * @param {Vector} vector the vector to compare equality to
	 * @return true if this Vector equals the parameter Vector.
	 */
	this.equals = function(vector) {
	    if(this.x === vector.x && this.y === vector.y) {
	        return true;
	    }
	    
	    else {
	        return false;
	    }
	}
}