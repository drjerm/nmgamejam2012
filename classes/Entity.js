//--------------------------------------------------------------------------
//------------- Entity -----------------------------------------------------
//--------------------------------------------------------------------------
function Entity(_screen, _pos, r)
{	
    this.ID  = _screen.GenerateObjectID(); // so it can find itself in the global object array
	this.pos = new Vector(_pos.x,_pos.y); //position vector
	this.rot = r;
}