//--------------------------------------------------------------------------
//------------- Game Level -------------------------------------------------
//--------------------------------------------------------------------------
function GameLevel(_screen)
{		
		Level.call(this, _screen);

		this.colliderArray 		= new Array(); 	// Contains all Collidable Sprites 
		this.characterArray 	= new Array();	// Contains all Characters 
		this.controllerArray 	= new Array();	// Contains all Controllers
		//this.botControllerArray = new Array();
		this.spawnerArray		= new Array(); 	// Contains all Spawners

	// --- Internal representation of the GameLevel

	this.world = new NodeMap(4096,4096, _screen);
	
	$.get('Level.txt', function(data){
	   _screen.level.loadLevel(data);
	});
	
	this.loadLevel = function(data)
	{	
		
		
		this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-832,1728),1.5707963267948966, this.MasterArray[24]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-832,1104),0, this.MasterArray[24]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-832,1600),1.5707963267948966*3, this.MasterArray[25]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-832,1472),1.5707963267948966, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-832,1344),1.5707963267948966, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-832,1216),1.5707963267948966, this.MasterArray[23]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-960,1104),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1088,1104),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1216,1104),0, this.MasterArray[23]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-960,1728),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1088,1728),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1216,1728),0, this.MasterArray[23]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-704,1600),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-576,1600),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-448,1600),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-320,1600),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-192,1600),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-64,1600),0, this.MasterArray[23]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(64,1600),Math.PI/2, this.MasterArray[25]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(64,1472),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(64,1344),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(64,1728),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(64,1856),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(64,1984),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(64,2112),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(64,2240),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(64,2368),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(64,2496),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(64,2624),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(64,2752),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(64,2880),(3*Math.PI/2), this.MasterArray[25]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(64,3008),Math.PI/2, this.MasterArray[23]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(192,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(320,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(448,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(576,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(704,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(832,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(960,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1088,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1216,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1344,2880),Math.PI, this.MasterArray[25]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1472,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1600,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1728,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1856,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1984,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2112,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2240,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2368,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2496,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2624,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2752,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2880,2880),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,2880),Math.PI/2, this.MasterArray[24]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1344,2752),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1344,2624),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1344,2368),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1344,2496),Math.PI/2, this.MasterArray[23]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,1472),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,1344),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,1728),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,1856),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,1984),Math.PI/2, this.MasterArray[25]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2880,1984),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2752,1984),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2624,1984),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2496,1984),Math.PI, this.MasterArray[24]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2496,1984),Math.PI, this.MasterArray[24]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2496,1856),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2496,1728),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2496,1600),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2496,1472),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2496,1344),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2496,1216),0, this.MasterArray[24]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2368,1216),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2240,1216),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2112,1216),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1984,1216),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1856,1216),3*Math.PI/2, this.MasterArray[24]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1856,1344),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1856,1472),Math.PI/2, this.MasterArray[24]);


this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(576,1472),Math.PI, this.MasterArray[24]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(704,1472),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(832,1472),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(960,1472),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1088,1472),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1216,1472),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1344,1472),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1472,1472),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1600,1472),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1728,1472),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(576,1344),Math.PI/2, this.MasterArray[23]);



this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,2112),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,2240),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,2368),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,2496),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,2624),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,2752),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,1600),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,1344),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,1216),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,1088),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,960),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,832),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,704),Math.PI/2, this.MasterArray[25]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2880,704),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2752,704),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2624,704),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2496,704),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2368,704),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2240,704),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2112,704),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1984,704),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1856,704),0, this.MasterArray[23]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,576),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,448),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,320),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,192),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,64),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,-64),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,-448),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,-320),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(3008,-192),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2880,-704),0, this.MasterArray[26]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2624,-896),0, this.MasterArray[27]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1936,-720),0, this.MasterArray[28]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(688,1680),0, this.MasterArray[0]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1136,2592),0, this.MasterArray[0]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-512,-256),0, this.MasterArray[0]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1616,-720),3*Math.PI/2, this.MasterArray[25]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1616,-592),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1616,-848),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1616,-976),0, this.MasterArray[24]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1488,-976),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1104,-976),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(976,-976),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(848,-976),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(720,-976),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(592,-976),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(464,-976),3*Math.PI/2, this.MasterArray[24]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(464,-848),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(464,-720),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(464,-592),Math.PI/2, this.MasterArray[24]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(336,-592),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(208,-592),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(80,-592),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-48,-592),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-176,-592),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-304,-592),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-432,-592),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-560,-592),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-688,-592),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-816,-592),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-944,-592),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1072,-592),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1200,-592),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1328,-592),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1456,-592),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1584,-592),3*Math.PI/2, this.MasterArray[24]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1584,-464),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1584,-336),Math.PI/2, this.MasterArray[24]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1712,-336),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1840,-336),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1968,-336),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2096,-336),3*Math.PI/2, this.MasterArray[24]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2096,-208),Math.PI/2, this.MasterArray[23]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(695,-2618),0, this.MasterArray[34]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(637,-1905),0, this.MasterArray[35]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-434,-2358),0, this.MasterArray[36]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2704,-1968),0, this.MasterArray[30]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-384,2432),0, this.MasterArray[0]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-384,2688),0, this.MasterArray[0]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1840,1728),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1968,1728),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2096,1728),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2224,1728),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2352,1728),3*Math.PI/2, this.MasterArray[25]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1840,1088),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1968,1088),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2096,1088),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2224,1088),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2352,1088),0, this.MasterArray[25]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2352,1216),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2352,1344),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2352,1472),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2352,1600),Math.PI/2, this.MasterArray[23]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2352,1856),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2352,1984),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2352,2112),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2352,2240),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2352,2368),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2352,2496),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2352,2624),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2352,2752),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2352,2880),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2352,3008),Math.PI/2, this.MasterArray[24]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2480,3008),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2608,3008),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2736,3008),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2864,3008),0, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,3008),0, this.MasterArray[24]);


this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,2752),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,2624),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,2496),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,2368),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,2240),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,2112),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,1984),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,1856),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,1728),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,1600),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,1472),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,1344),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,1216),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,1088),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,960),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,832),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,704),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,576),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,448),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,320),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,192),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,2880),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,64),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,-64),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,-192),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,-320),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,-448),Math.PI/2, this.MasterArray[23]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2992,-576),Math.PI/2, this.MasterArray[23]);

this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-2672,496),0, this.MasterArray[29]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1869.9166362780472,-66.73008888865449),0, this.MasterArray[18]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-655.5399712788217,410.0410058249754),0, this.MasterArray[18]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-1924.6264323684663,2835.8383106843257),0, this.MasterArray[15]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(-992.464062756261,2833.700323712399),0, this.MasterArray[15]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(1582.493032015003,2399.728560782304),0, this.MasterArray[3]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(356.22132669653206,2608.395431437953),0, this.MasterArray[6]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2781.7631933587013,2636.2485031408096),0, this.MasterArray[1]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2038.978712660335,2643.5097716915775),0, this.MasterArray[2]);
this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(2796.3308679925044,-1623.277683362841),0, this.MasterArray[10]);

this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(-300, -1182) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(-690, -900) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(700, -1200) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(-1500, 1380) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(900, 2300) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(2400, 2300) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(2750, 1650) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(-300, 1890) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(1940, -2340) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(-2000, -2300) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(-2022, 2130) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(2472, 2370) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(200, 200) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(-400, 200) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(954, 630) );

this.spawnerArray[this.spawnerArray.length] = new BotSpawner(_screen,new Vector(1900, 942) );
this.spawnerArray[this.spawnerArray.length] = new BotSpawner(_screen,new Vector(300, 1422) );
this.spawnerArray[this.spawnerArray.length] = new BotSpawner(_screen,new Vector(-2040, 1422) );
this.spawnerArray[this.spawnerArray.length] = new BotSpawner(_screen,new Vector(-2760, 1914) );
this.spawnerArray[this.spawnerArray.length] = new BotSpawner(_screen,new Vector(-2300, -2080) );
this.spawnerArray[this.spawnerArray.length] = new BotSpawner(_screen,new Vector(672, 2256) );
this.spawnerArray[this.spawnerArray.length] = new BotSpawner(_screen,new Vector(-132, 2256) );

this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(-2868, 1944) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(2778, 1794) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(-1272, -360) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(2262, -1128) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(2430, -672) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(2730, 2364) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(-800, 1974) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(2526, 2460) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(2226, 1482) );

this.spawnerArray[this.spawnerArray.length] = new BotSpawner(_screen,new Vector(2184, 2154) );
this.spawnerArray[this.spawnerArray.length] = new BotSpawner(_screen,new Vector(-900, 2154) );
this.spawnerArray[this.spawnerArray.length] = new BotSpawner(_screen,new Vector(100, 600) );
this.spawnerArray[this.spawnerArray.length] = new BotSpawner(_screen,new Vector(-100, 600) );
this.spawnerArray[this.spawnerArray.length] = new BotSpawner(_screen,new Vector(-1800, -1200) );
this.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(-2700,2500) );






this.spriteArray[this.spriteArray.length]=new Sprite(_screen,new Vector(226,-2354),0, this.MasterArray[37]);
		
		
		//if(this.characterArray.length > 0);
		//{
			_screen.takeControlOfCharacter(this.characterArray[this.characterArray.length -1 ]);
		//}
		
		_screen.Setup();
	}
	
	
	
	this.generateLevelString = function()
	{
	    var STRING = "";
		var r = "";
		var imgsrc = null;
		
		for (i in this.spriteArray) 
		{
			s = this.spriteArray[i];
			if(s instanceof(Player))
				STRING = STRING + "\nthis.spawnerArray[this.spawnerArray.length] = new PlayerSpawner(_screen,new Vector(" + s.pos.x+ "," + s.pos.y + ") );";
			else if(s instanceof(Bot))
				STRING = STRING + "\nthis.spawnerArray[this.spawnerArray.length] = new BotSpawner(_screen,new Vector(" + s.pos.x+ "," + s.pos.y + ") );";
			else if(s instanceof(Building))
				STRING = STRING + "\nthis.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(" + s.pos.x + "," + s.pos.y + ")," + s.rot + ", this.MasterArray[" +s.img.src.slice(s.img.src.search("images")) + "]);";
			

		}
		alert(STRING);
	}
    
    
    
    
    //This entire block is here for testing the NodeMap.js.  Please do not remove yet. --Rogelio 
    
    var testPos;
    var nodeCoordForPos;
    var posForNodeCoord;
    testPos = new Vector(128,1024);
    nodeCoordForPos = this.world.getNodeCoordinateForPosition(testPos);
    console.log("Node Coordinate for Position: (" + testPos.x + "," +testPos.y+") is: (" + nodeCoordForPos.x + "," + nodeCoordForPos.y +")");
        
    posForNodeCoord = this.world.getCenterPositionForNodeCoordinates(nodeCoordForPos.x,nodeCoordForPos.y);
    console.log("Position for Node Coordinate: (" + nodeCoordForPos.x + "," +nodeCoordForPos.y+") is: (" + posForNodeCoord.x + "," + posForNodeCoord.y +")");
        
    testPos = new Vector(-666,666);
    nodeCoordForPos = this.world.getNodeCoordinateForPosition(testPos);
    console.log("Node Coordinate for Position: (" + testPos.x + "," +testPos.y+") is: (" + nodeCoordForPos.x + "," + nodeCoordForPos.y +")");
        
    posForNodeCoord = this.world.getCenterPositionForNodeCoordinates(nodeCoordForPos.x,nodeCoordForPos.y);
    console.log("Position for Node Coordinate: (" + nodeCoordForPos.x + "," +nodeCoordForPos.y+") is: (" + posForNodeCoord.x + "," + posForNodeCoord.y +")");
       
    testPos = new Vector(1337,1337);
    nodeCoordForPos = this.world.getNodeCoordinateForPosition(testPos);
    console.log("Node Coordinate for Position: (" + testPos.x + "," +testPos.y+") is: (" + nodeCoordForPos.x + "," + nodeCoordForPos.y +")");
        
    posForNodeCoord = this.world.getCenterPositionForNodeCoordinates(nodeCoordForPos.x,nodeCoordForPos.y);
    console.log("Position for Node Coordinate: (" + nodeCoordForPos.x + "," +nodeCoordForPos.y+") is: (" + posForNodeCoord.x + "," + posForNodeCoord.y +")");
       
    testPos = new Vector(1234,-2000);
    nodeCoordForPos = this.world.getNodeCoordinateForPosition(testPos);
    console.log("Node Coordinate for Position: (" + testPos.x + "," +testPos.y+") is: (" + nodeCoordForPos.x + "," + nodeCoordForPos.y +")");
        
    posForNodeCoord = this.world.getCenterPositionForNodeCoordinates(nodeCoordForPos.x,nodeCoordForPos.y);
    console.log("Position for Node Coordinate: (" + nodeCoordForPos.x + "," +nodeCoordForPos.y+") is: (" + posForNodeCoord.x + "," + posForNodeCoord.y +")");
        
     testPos = new Vector(-311,-113);
    nodeCoordForPos = this.world.getNodeCoordinateForPosition(testPos);
    console.log("Node Coordinate for Position: (" + testPos.x + "," +testPos.y+") is: (" + nodeCoordForPos.x + "," + nodeCoordForPos.y +")");
        
    posForNodeCoord = this.world.getCenterPositionForNodeCoordinates(nodeCoordForPos.x,nodeCoordForPos.y);
    console.log("Position for Node Coordinate: (" + nodeCoordForPos.x + "," +nodeCoordForPos.y+") is: (" + posForNodeCoord.x + "," + posForNodeCoord.y +")");
      
    
   
    

		//Load content. Should go into a content loader
	this.backgroundImage = { src: ["images/cthulumap001.jpg",
	                            "images/cthulumap002.jpg","images/cthulumap003.jpg","images/cthulumap004.jpg",
	                            		"images/cthulumap005.jpg","images/cthulumap006.jpg",
	                            		"images/cthulumap007.jpg","images/cthulumap008.jpg","images/cthulumap009.jpg"],
	                            		width: 2048,
	                            		height: 2048};
	
	this.peasent1 = { src: "images/spritesheets/PeasentTotalAnims2.png", 
			height: 128, width: 128, map : THREE.ImageUtils.loadTexture("images/spritesheets/PeasentTotalAnims.png"),
			size: 128};
	
	this.peasentInd = { src: "images/spritesheets/IndoctrinatedWalkCycle.png", 
			height: 128, width: 128, map : THREE.ImageUtils.loadTexture("images/spritesheets/IndocWalkTotal.png"),
			size: 128};
	
	this.peasentExp = { src: "images/spritesheets/indocLossOfBodilyFunctionSPRITE.png", 
			height: 128, width: 128, map : THREE.ImageUtils.loadTexture("images/spritesheets/indocLossOfBodilyFunctionSPRITE.png"),
			size: 128};
	
//	this.peasent1.idleAnim = new TextureAnimator(this.peasent1.src, 8, 4, 128, 3, 1, 75 );
	this.scrIntro = new Image();
	this.scrIntro.src = "images/cthululoader.png";
	this.scrIntro.width = 1920;
	this.scrIntro.height = 1080;
	this.scrIntro.map = THREE.ImageUtils.loadTexture( this.scrIntro.src);
	
	this.scrEndWin = new Image();
	this.scrEndWin.src = "images/CthulhuEndWinning.png";
	this.scrEndWin.width = 1920;
	this.scrEndWin.height = 1080;
	this.scrEndWin.map = THREE.ImageUtils.loadTexture( this.scrEndWin.src);
	
	this.peasentPos = new Image();
	this.peasentPos.src = "images/spritesheets/PosessedTotalAnims.png";
	this.peasentPos.width = 128;
	this.peasentPos.height = 128;
	this.peasentPos.map = THREE.ImageUtils.loadTexture( this.peasentPos.src );
	
	this.guard1 = new Image();
	this.guard1.src = "images/spritesheets/Guard_w_Armor_Blue_Spreadsheet.png";
	this.guard1.width = 128;
	this.guard1.height = 128;
	this.guard1.map = THREE.ImageUtils.loadTexture( this.guard1.src);
	
	this.guard2 = new Image();
	this.guard2.src = "images/spritesheets/Guard_w_Armor_Green_Spreadsheet.png";
	this.guard2.width = 128;
	this.guard2.height = 128;
	this.guard2.map = THREE.ImageUtils.loadTexture( this.guard2.src);
	
	this.guard3 = new Image();
	this.guard3.src = "images/spritesheets/Guard_w_Armor_Red_Spreadsheet.png";
	this.guard3.width = 128;
	this.guard3.height = 128;
	this.guard3.map = THREE.ImageUtils.loadTexture( this.guard3.src);
	
	this.guard4 = new Image();
	this.guard4.src = "images/spritesheets/Guard_w_Armor_Orange_Spreadsheet.png";
	this.guard4.width = 128;
	this.guard4.height = 128;
	this.guard4.map = THREE.ImageUtils.loadTexture( this.guard4.src);
	
	this.guardArray = [this.guard1, this.guard2, this.guard3, this.guard4];
	
	this.tx_null = new Image();
	this.tx_null.src = "images/img_null.png";
	this.tx_null.width = 25;
	this.tx_null.height = 25;
	this.tx_null.map = THREE.ImageUtils.loadTexture( this.tx_null.src);
	
	this.tx_player1 = new Image();
	this.tx_player1.src = "images/img_player1.png";
	this.tx_player1.width = 25;
	this.tx_player1.height = 25;
	this.tx_player1.map = THREE.ImageUtils.loadTexture( this.tx_player1.src);
	
	this.tx_player2 = new Image();
	this.tx_player2.src = "images/img_player2.png";
	this.tx_player2.width = 25;
	this.tx_player2.height = 25;
	this.tx_player2.map = THREE.ImageUtils.loadTexture( this.tx_player2.src);
	
	this.tx_zombie = new Image();
	this.tx_zombie.src = "images/img_zombie.png";
	this.tx_zombie.width = 25;
	this.tx_zombie.height = 25;
	this.tx_zombie.map = THREE.ImageUtils.loadTexture( this.tx_zombie.src);
	
	
	this.tx_buildingTest = new Image();
	this.tx_buildingTest.src = "images/Testbuilding.png";
	this.tx_buildingTest.width = 64;
	this.tx_buildingTest.height = 128;
	this.tx_buildingTest.map = THREE.ImageUtils.loadTexture( this.tx_buildingTest.src);

	this.tx_buildingSquare01 = new Image();
	this.tx_buildingSquare01.src = "images/Rooftop_1.jpg";
	this.tx_buildingSquare01.width = 256;
	this.tx_buildingSquare01.height = 256;
	this.tx_buildingSquare01.map = THREE.ImageUtils.loadTexture( this.tx_buildingSquare01.src);
	
	this.tx_buildingSquare02 = new Image();
	this.tx_buildingSquare02.src = "images/Shop2.jpg";
	this.tx_buildingSquare02.width = 256;
	this.tx_buildingSquare02.height = 256;
	this.tx_buildingSquare02.map = THREE.ImageUtils.loadTexture( this.tx_buildingSquare02.src);
	
	this.tx_buildingSquare03 = new Image();
	this.tx_buildingSquare03.src = "images/Shop3.jpg";
	this.tx_buildingSquare03.width = 256;
	this.tx_buildingSquare03.height = 256;
	this.tx_buildingSquare03.map = THREE.ImageUtils.loadTexture( this.tx_buildingSquare03.src);
	
	this.tx_buildingSquare04 = new Image();
	this.tx_buildingSquare04.src = "images/Shop4.jpg";
	this.tx_buildingSquare04.width = 256;
	this.tx_buildingSquare04.height = 256;
	this.tx_buildingSquare04.map = THREE.ImageUtils.loadTexture( this.tx_buildingSquare04.src);
	
	this.tx_buildingSquare05 = new Image();
	this.tx_buildingSquare05.src = "images/Shop5.jpg";
	this.tx_buildingSquare05.width = 256;
	this.tx_buildingSquare05.height = 256;
	this.tx_buildingSquare05.map = THREE.ImageUtils.loadTexture( this.tx_buildingSquare05.src);
	
	this.tx_buildingSquare06 = new Image();
	this.tx_buildingSquare06.src = "images/Shop6.jpg";
	this.tx_buildingSquare06.width = 256;
	this.tx_buildingSquare06.height = 256;
	this.tx_buildingSquare06.map = THREE.ImageUtils.loadTexture( this.tx_buildingSquare06.src);
	
	this.tx_buildingSquare07 = new Image();
	this.tx_buildingSquare07.src = "images/Shop7.jpg";
	this.tx_buildingSquare07.width = 256;
	this.tx_buildingSquare07.height = 256;
	this.tx_buildingSquare07.map = THREE.ImageUtils.loadTexture( this.tx_buildingSquare07.src);
	
	this.tx_buildingSquare08 = new Image();
	this.tx_buildingSquare08.src = "images/Shop8.jpg";
	this.tx_buildingSquare08.width = 256;
	this.tx_buildingSquare08.height = 256;
	this.tx_buildingSquare08.map = THREE.ImageUtils.loadTexture( this.tx_buildingSquare08.src);
	
	this.tx_buildingSquare09 = new Image();
	this.tx_buildingSquare09.src = "images/Shop9.jpg";
	this.tx_buildingSquare09.width = 256;
	this.tx_buildingSquare09.height = 256;
	this.tx_buildingSquare09.map = THREE.ImageUtils.loadTexture( this.tx_buildingSquare09.src);
	
	this.tx_buildingSquare10 = new Image();
	this.tx_buildingSquare10.src = "images/thatch_hut.png";
	this.tx_buildingSquare10.width = 500;
	this.tx_buildingSquare10.height = 500;
	this.tx_buildingSquare10.map = THREE.ImageUtils.loadTexture( this.tx_buildingSquare10.src);
	
	this.tx_buildingSquare11 = new Image();
	this.tx_buildingSquare11.src = "images/windmill2.png";
	this.tx_buildingSquare11.width = 256;
	this.tx_buildingSquare11.height = 256;
	this.tx_buildingSquare11.map = THREE.ImageUtils.loadTexture( this.tx_buildingSquare11.src);
	
	this.tx_buildingLong01 = new Image();
	this.tx_buildingLong01.src = "images/fadedbuilding.png";
	this.tx_buildingLong01.width = 512;
	this.tx_buildingLong01.height = 279;
	this.tx_buildingLong01.map = THREE.ImageUtils.loadTexture( this.tx_buildingLong01.src);
	
	this.tx_buildingLong02 = new Image();
	this.tx_buildingLong02.src = "images/JapaneseHut.png";
	this.tx_buildingLong02.width = 250;
	this.tx_buildingLong02.height = 161;
	this.tx_buildingLong02.map = THREE.ImageUtils.loadTexture( this.tx_buildingLong02.src);
	
	this.tx_buildingLong03 = new Image();
	this.tx_buildingLong03.src = "images/logroof2.png";
	this.tx_buildingLong03.width = 400;
	this.tx_buildingLong03.height = 322;
	this.tx_buildingLong03.map = THREE.ImageUtils.loadTexture( this.tx_buildingLong03.src);
	
	this.tx_buildingLong04 = new Image();
	this.tx_buildingLong04.src = "images/logroof3.png";
	this.tx_buildingLong04.width = 400;
	this.tx_buildingLong04.height = 322;
	this.tx_buildingLong04.map = THREE.ImageUtils.loadTexture( this.tx_buildingLong04.src);

	
	this.tx_buildingLong05 = new Image();
	this.tx_buildingLong05.src = "images/roof9.png";
	this.tx_buildingLong05.width = 512;
	this.tx_buildingLong05.height = 279;
	this.tx_buildingLong05.map = THREE.ImageUtils.loadTexture( this.tx_buildingLong05.src);
	
	this.tx_buildingLong06 = new Image();
	this.tx_buildingLong06.src = "images/Shop.jpg";
	this.tx_buildingLong06.width = 512;
	this.tx_buildingLong06.height = 418;
	this.tx_buildingLong06.map = THREE.ImageUtils.loadTexture( this.tx_buildingLong06.src);
	
	this.tx_buildingSide01 = new Image();
	this.tx_buildingSide01.src = "images/fadedbuildingSide.png";
	this.tx_buildingSide01.height = 512;
	this.tx_buildingSide01.width = 279;
	this.tx_buildingSide01.map = THREE.ImageUtils.loadTexture( this.tx_buildingSide01.src);
	
	this.tx_buildingSide02 = new Image();
	this.tx_buildingSide02.src = "images/JapaneseHutSide.png";
	this.tx_buildingSide02.height = 250;
	this.tx_buildingSide02.width = 161;
	this.tx_buildingSide02.map = THREE.ImageUtils.loadTexture( this.tx_buildingSide02.src);
	
	this.tx_buildingSide03 = new Image();
	this.tx_buildingSide03.src = "images/logroof2Side.png";
	this.tx_buildingSide03.height = 400;
	this.tx_buildingSide03.width = 322;
	this.tx_buildingSide03.map = THREE.ImageUtils.loadTexture( this.tx_buildingSide03.src);
	
	this.tx_buildingSide04 = new Image();
	this.tx_buildingSide04.src = "images/logroof3Side.png";
	this.tx_buildingSide04.height = 400;
	this.tx_buildingSide04.width = 322;
	this.tx_buildingSide04.map = THREE.ImageUtils.loadTexture( this.tx_buildingSide04.src);

	
	this.tx_buildingSide05 = new Image();
	this.tx_buildingSide05.src = "images/roof9Side.png";
	this.tx_buildingSide05.height = 512;
	this.tx_buildingSide05.width = 279;
	this.tx_buildingSide05.map = THREE.ImageUtils.loadTexture( this.tx_buildingSide05.src);
	
	this.tx_buildingSide06 = new Image();
	this.tx_buildingSide06.src = "images/ShopSide.jpg";
	this.tx_buildingSide06.height = 512;
	this.tx_buildingSide06.width = 418;
	this.tx_buildingSide06.map = THREE.ImageUtils.loadTexture( this.tx_buildingSide06.src);
	
	this.tx_WallStraight = new Image();
	this.tx_WallStraight.src = "images/wall_straight.png";
	this.tx_WallStraight.height = 128;
	this.tx_WallStraight.width = 128;
	this.tx_WallStraight.map = THREE.ImageUtils.loadTexture( this.tx_WallStraight.src);
	
	this.tx_WallCorner = new Image();
	this.tx_WallCorner.src = "images/corner.png";
	this.tx_WallCorner.height = 128;
	this.tx_WallCorner.width = 128;
	this.tx_WallCorner.map = THREE.ImageUtils.loadTexture( this.tx_WallCorner.src);
	
	this.tx_WallIntersect = new Image();
	this.tx_WallIntersect.src = "images/tJunct.png";
	this.tx_WallIntersect.height = 128;
	this.tx_WallIntersect.width = 128;
	this.tx_WallIntersect.map = THREE.ImageUtils.loadTexture( this.tx_WallIntersect.src);
	
	this.tx_GuardTower = new Image();
	this.tx_GuardTower.src = "images/guardTower.png";
	this.tx_GuardTower.height = 384;
	this.tx_GuardTower.width = 384;
	this.tx_GuardTower.map = THREE.ImageUtils.loadTexture( this.tx_GuardTower.src);
	
	
	this.tx_Market = new Image();
	this.tx_Market.src = "images/market_blueRed.png";
	this.tx_Market.width = 500;
	this.tx_Market.height = 250;
	this.tx_Market.map = THREE.ImageUtils.loadTexture( this.tx_Market.src);
	
	this.tx_Temple = new Image();
	this.tx_Temple.src = "images/temple.png";
	this.tx_Temple.width = 702;
	this.tx_Temple.height = 1149;
	this.tx_Temple.map = THREE.ImageUtils.loadTexture( this.tx_Temple.src);
	
	this.tx_Tree01 = new Image();
	this.tx_Tree01.src = "images/Cthulu_tree_001.png";
	this.tx_Tree01.height = 128;
	this.tx_Tree01.width = 128;
	this.tx_Tree01.map = THREE.ImageUtils.loadTexture( this.tx_Tree01.src);
	
	this.tx_Tree02 = new Image();
	this.tx_Tree02.src = "images/Cthulu_tree_002.png";
	this.tx_Tree02.height = 128;
	this.tx_Tree02.width = 128;
	this.tx_Tree02.map = THREE.ImageUtils.loadTexture( this.tx_Tree02.src);
	
	this.tx_Tree03 = new Image();
	this.tx_Tree03.src = "images/Cthulu_tree_003.png";
	this.tx_Tree03.height = 256;
	this.tx_Tree03.width = 256;
	this.tx_Tree03.map = THREE.ImageUtils.loadTexture( this.tx_Tree03.src);
	
this.tx_cave1 = new Image();
	this.tx_cave1.src = "images/cavecol001.png";
	this.tx_cave1.width = 662;
	this.tx_cave1.height = 905;
	this.tx_cave1.map = THREE.ImageUtils.loadTexture( this.tx_cave1.src);
	
	this.tx_cave2 = new Image();
	this.tx_cave2.src = "images/cavecol002.png";
	this.tx_cave2.width = 778;
	this.tx_cave2.height = 524;
	this.tx_cave2.map = THREE.ImageUtils.loadTexture( this.tx_cave2.src);
	
	this.tx_cave3 = new Image();
	this.tx_cave3.src = "images/cavecol003.png";
	this.tx_cave3.width = 700;
	this.tx_cave3.height = 1436;
	this.tx_cave3.map = THREE.ImageUtils.loadTexture( this.tx_cave3.src);
	this.tx_GateOpen = new Image();
	this.tx_GateOpen.src = "images/gateOpen.png";
	this.tx_GateOpen.width = 128;
	this.tx_GateOpen.height = 512;
	this.tx_GateOpen.map = THREE.ImageUtils.loadTexture( this.tx_GateOpen.src);
	
	this.tx_Gate = new Image();
	this.tx_Gate.src = "images/gate.png";
	this.tx_Gate.width = 512;
	this.tx_Gate.height = 128;
	this.tx_Gate.map = THREE.ImageUtils.loadTexture( this.tx_Gate.src);

	this.tx_necroRing1 = new Image();
	this.tx_necroRing1.src = "images/necroRing_1.png";
	this.tx_necroRing1.height = 256;
	this.tx_necroRing1.width = 256;
	this.tx_necroRing1.map = THREE.ImageUtils.loadTexture( this.tx_necroRing1.src);
	
	this.tx_necroRing2 = new Image();
	this.tx_necroRing2.src = "images/necroRing_2.png";
	this.tx_necroRing2.height = 256;
	this.tx_necroRing2.width = 256;
	this.tx_necroRing2.map = THREE.ImageUtils.loadTexture( this.tx_necroRing2.src);
this.tx_ct = new Image();
	this.tx_ct.src = "images/cthulhu head overlay.png";
	this.tx_ct.height = 1444;
	this.tx_ct.width = 1036;
	this.tx_ct.map = THREE.ImageUtils.loadTexture( this.tx_ct.src);
	
	this.MasterArray = new Array();
	this.MasterArray[0] = this.tx_buildingSquare01;
	this.MasterArray[1] = this.tx_buildingSquare02;
	this.MasterArray[2] = this.tx_buildingSquare03;
	this.MasterArray[3] = this.tx_buildingSquare04;
	this.MasterArray[4] = this.tx_buildingSquare05;
	this.MasterArray[5] = this.tx_buildingSquare06;
	this.MasterArray[6] = this.tx_buildingSquare07;
	this.MasterArray[7] = this.tx_buildingSquare08;
	this.MasterArray[8] = this.tx_buildingSquare09;
	this.MasterArray[9] = this.tx_buildingSquare10;
	this.MasterArray[10] = this.tx_buildingSquare11;
	
	this.MasterArray[11] = this.tx_buildingLong01;
	this.MasterArray[12] = this.tx_buildingLong02;
	this.MasterArray[13] = this.tx_buildingLong03;
	this.MasterArray[14] = this.tx_buildingLong04;
	this.MasterArray[15] = this.tx_buildingLong05;
	this.MasterArray[16] = this.tx_buildingLong06;
	
	this.MasterArray[17] = this.tx_buildingSide01;
	this.MasterArray[18] = this.tx_buildingSide02;
	this.MasterArray[19] = this.tx_buildingSide03;
	this.MasterArray[20] = this.tx_buildingSide04;
	this.MasterArray[21] = this.tx_buildingSide05;
	this.MasterArray[22] = this.tx_buildingSide06;
	
	this.MasterArray[23] = this.tx_WallStraight;
	this.MasterArray[24] = this.tx_WallCorner;
	this.MasterArray[25] = this.tx_WallIntersect;
	this.MasterArray[26] = this.tx_GuardTower;
	this.MasterArray[27] = this.tx_GateOpen;
	this.MasterArray[28] = this.tx_Gate;
	
	this.MasterArray[29] = this.tx_Market;
	this.MasterArray[30] = this.tx_Temple;
	
	
	
	this.MasterArray[31] = this.tx_Tree01;
	this.MasterArray[32] = this.tx_Tree02;
	this.MasterArray[33] = this.tx_Tree03;
	
	this.MasterArray[34] = this.tx_cave1;
	this.MasterArray[35] = this.tx_cave2;
	this.MasterArray[36] = this.tx_cave3;
	this.MasterArray[37] = this.tx_ct;	
	


		this.assets = [this.backgroundImage, this.tx_null, this.tx_player1, this.tx_player2, this.tx_zombie, this.tx_buildingTest];

}