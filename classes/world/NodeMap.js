
/**
 * Creates a node-based Map representation of the game world.  The NodeMap representation
 * mirrors the game world physicality in that:
 *  1) it defines positive indices for array positions that expand to the top/right portions 
 *  2) it defines negative indices for array positions that expand to the bottom/left portions
 * of the map.   
 * 
 * Each node in the NodeMap has a value that provides information used exclusively for
 * AI pathfinding.  If the node contains a zero for its value, the node is not available
 * for pathfinding; otherwise, it is.
 * @param {Number} height the height of the map in pixels 
 * @param {Number} width the width of the map in pixels
 * @param {Screen} the screen of interest
 */
function NodeMap(height, width, _screen)
{
    // The NodeMap is an abstraction of the physical game world. It
    // overlays on the game world a grid structure useful for AI
    // pathfinding.
    //
    // Because the NodeMap cannot represent in one block the center of
    // the Cartesian Coordinate plane (which is in the exact center of
    // the screen), the NodeMap inserts an additional row and column,
    // which act like the dummy center of the NodeMap.
    //
    //              ^
    //              |
    //              |
    //              |[+]
    //              |[+][+]
    //     <------------------>
    //        [-][-]|
    //           [-]|
    //              |
    //              |
    //              V
    //
    // There are no NodeMap indices that can reach the exact center, 
    // and as such, (with respect to the center) the NodeMap array
    // begins at 1 (toward the positive ends) 
    // or -1 (toward the negative ends)
    
    this.tileHeight   = 32; //px
    this.tileWidth    = 32; //px
    this.numberOfArrayRows    = (height / this.tileHeight) + 1;  //add the dummy row
    this.numberOfArrayColumns = (width  / this.tileWidth) + 1;   //add the dummy column
    this.dimension = this.numberOfArrayRows;
        
    this.nodeMap = createSquareArray(this.dimension);
        //Initially, all array indices in the NodeMap are available for pathfinding.
         
    this.relativeZeroRow = ((this.numberOfArrayRows-1)/ 2);
    this.relativeZeroCol = ((this.numberOfArrayColumns-1) / 2);



    /**
     * Returns the value in the NodeMap for the given relative NodeMap coordinates.
     * (Relative coordinates are always taken with respect to the center of the screen)
     * @param {Number} relativeRow the row index of the NodeMap relative to the center of the screen
     * @param {Number} relativeCol the column index of the NodeMap relative to the center of the screen
     * @return {Number} the value in the node map that corresponds to the parameter position

     */
    this.getNodeValueForNodeCoordinates = function(relativeRow, relativeCol)
    {
        if(relativeRow === 0 && relativeCol === 0) {
            return this.nodeMap[this.relativeZeroRow][this.relativeZeroCol];
        }
        
         // else if(relativeRow === 0 || relativeCol === 0) {
            // throw "NodeMap.js: Cannot find position for dummy row/column.";
        // }
        
        return this.nodeMap[this.relativeZeroRow+relativeRow][this.relativeZeroCol+relativeCol];        
    }



    /**
     * Returns the node's value at the paramter physical game map position.
     * @param {Vector} position the physical position on the game map
     * @return {Number} the value in the node map that corresponds to the parameter position
     */
    this.getNodeValueForPosition = function(position)
    {
        nodeCoordinate = this.getNodeCoordinateForPosition(position);
        return this.nodeMap[this.relativeZeroRow + nodeCoordinate.y][this.relativeZeroCol + nodeCoordinate.x];
    }
    
    
    
    /**
     * Returns the node's NodeMap coordinates at the parameter physical game map position.
     * @param {Vector} position the physical position on the game map
     * @return {Vector} the index in the node map that corresponds to the parameter position
     */
    this.getNodeCoordinateForPosition = function(position)
    {
        if(position.x === 0 && position.y === 0) {
            return this.nodeMap[this.relativeZeroRow][this.relativeZeroCol];
        }
         
        var offsetWidth  = (position.x < 0) ? -1 : 1;
        var offsetHeight = (position.y < 0) ? -1 : 1;
        
        var row = Math.floor(position.x/this.tileHeight) + offsetHeight;
        var col = Math.floor(position.y/this.tileWidth)  + offsetWidth; 
            //integer division, get the respective index and add 1 to account for
            //the dummy row and column
        
        return (new Vector(row,col)); 
        
    }
    
    
    
    /**
     * Sets the node's value at the parameter physical game map position.
     * @param {Number} value the value to assign to the node at the parameter position
     * @param {Vector} position the physical position on the game map
     */
    this.setNodeValueForPosition = function(value, position)
    {
        //handle the special case of 0,0:
        if(position.x === 0 && position.y === 0) {
            this.nodeMap[this.relativeZeroRow][this.relativeZeroCol];
            return;
        }
        
        var offsetWidth  = (position.x < 0) ? -1 : 1;
        var offsetHeight = (position.y < 0) ? -1 : 1;
        
        var row = Math.floor(position.x/this.tileHeight) + offsetHeight;
        var col = Math.floor(position.y/this.tileWidth)  + offsetWidth; 
            //integer division, get the respective index and add 1 to account for
            //the dummy row and column
        
        this.nodeMap[this.relativeZeroRow + row][this.relativeZeroCol + col];
    }
    
    
    
    /**
     * Gets the center coordinates for the node given by the parameters.
     * @param {Number} relativeRow the row index of the NodeMap relative to the center of the screen
     * @param {Number} relativeCol the column index of the NodeMap relative to the center of the screen
     * @return {Vector} the center coordinates of the node given by the parameters
     */
    this.getCenterPositionForNodeCoordinates = function(relativeRow,relativeCol)
    {
        //handle the special case of 0,0:
        if(relativeRow === 0 && relativeCol === 0) {
            return new Vector(0,0);
        }
        
        // else if(relativeRow === 0 || relativeCol === 0) {
            // throw "NodeMap.js: Cannot find position for dummy row/column.";
        // }
        
        var halfTileHeight = this.tileHeight/2;
        var halfTileWidth  = this.tileWidth/2;
        
        var offsetWidth  = (relativeCol > 0) ? 1 : -1;
        var offsetHeight = (relativeRow > 0) ? 1 : -1;
        
        var centerX = (relativeRow-offsetWidth) * this.tileWidth;
        var centerY = (relativeCol-offsetHeight) * this.tileHeight;
        
        centerX = (centerX < 0) ? (centerX - halfTileWidth) : (centerX + halfTileWidth);
        centerY = (centerY < 0) ? (centerY - halfTileHeight) : (centerY + halfTileHeight);
        
        return new Vector(centerX,centerY); 
        
    }
    
    
    
    /**
     * Converts the parameter absolute coordinates to relative ones (relative to the center).
     * @param {Integer} the absolute row index
     * @param {Integer} the absolute column index
     * @return a Vector containing the relative row,column indices
     */
    this.convertAbsoluteToRelativeNodeCoordinates = function(absoluteRow, absoluteCol)
    {
        if(absoluteRow === this.relativeZeroRow && absoluteCol === this.relativeZeroCol) {
            return new Vector(0,0);
        }
        
        else if(absoluteRow === this.relativeZeroRow || absoluteCol === this.relativeZeroCol) {
            return null;
        }
        
        return new Vector( (absoluteRow-this.relativeZeroRow) , (absoluteCol-this.relativeZeroCol ) );
    }
    
    
     // Initialization
    for(var rowIndex = 0; rowIndex < this.numberOfArrayRows; rowIndex++)
    {
        for(var colIndex = 0; colIndex < this.numberOfArrayColumns; colIndex++)
        {
            var relativeNodeCoords = this.convertAbsoluteToRelativeNodeCoordinates(rowIndex,colIndex);
            var centerPos = this.getCenterPositionForNodeCoordinates(relativeNodeCoords);

            this.nodeMap[rowIndex][colIndex] = new Entity(_screen, centerPos,0);
        }
    }
    
    for(var colIndex = 0; colIndex < this.numberOfArrayColumns; colIndex++) {
        this.nodeMap[this.relativeZeroRow][colIndex] = 0;
    } // The dummy row...
          
    for(var rowIndex = 0; rowIndex < this.numberOfArrayRows; rowIndex++) {
        this.nodeMap[rowIndex][this.relativeZeroCol] = 0;
    } // ...and dummy column are not available for pathfinding, and
      // have a special value to allow pathfinding to skip over them.
      // (normally, anything not available for pathfinding will be null)
     
    // ...but the exact center is a special case:
    this.nodeMap[this.relativeZeroRow][this.relativeZeroCol] = new Entity(_screen, new Vector(0,0),0);
            
  
}
