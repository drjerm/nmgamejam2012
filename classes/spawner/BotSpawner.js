//--------------------------------------------------------------------------
//------------- Bot Spawner ------------------------------------------------
//--------------------------------------------------------------------------
function BotSpawner(_screen, _pos)
{	
	
	Spawner.call(this,_screen, _pos);
	
	this.controller = new BotController(_screen); 
	
	this.random = Math.floor(Math.random() * _screen.level.guardArray.length);
	this.guardImage = _screen.level.guardArray[this.random];
	
	this.character = new Bot(_screen, new Vector(this.pos.x, this.pos.y), 0, this.guardImage);
	
	this.animation = new TextureAnimator(this.character.map, 8, 5, 1, 1, 150 );
	this.character.setAnimated(this.animation);
	
	this.controller.setCharacter(this.character);
	
	_screen.level.controllerArray[_screen.level.controllerArray.length]  = this.controller;
	_screen.level.spriteArray[_screen.level.spriteArray.length] = this.character;

}