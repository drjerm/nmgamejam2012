//--------------------------------------------------------------------------
//------------- Player Spawner ---------------------------------------------
//--------------------------------------------------------------------------
function PlayerSpawner(_screen, _pos)
{	
	Spawner.call(this,_screen, _pos);
	
	
	this.controller = new PlayerController(_screen); 
	this.character = new Player(_screen, new Vector(this.pos.x, this.pos.y), 0, _screen.level.peasent1);
	
	
	this.animation = new TextureAnimator(this.character.map, 8, 4, 3, 1, 150 );
	this.character.setAnimated(this.animation);
	
	
	this.controller.setCharacter(this.character);
	
	_screen.level.controllerArray[_screen.level.controllerArray.length]  = this.controller;
	_screen.level.spriteArray[_screen.level.spriteArray.length] = this.character;

}