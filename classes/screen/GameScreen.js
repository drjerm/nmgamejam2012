//--------------------------------------------------------------------------
//------------- GameScreen -------------------------------------------------
//--------------------------------------------------------------------------
//-- Default GameScreen. 
function GameScreen(_width,_height, level)
{	Screen.call(this,_width,_height);
	//-------- Initialized Vars ---------------------
	this.level = new GameLevel(this);
	this.playerID = 0; // Player ID		
	this.paused = false;
	this.cameraController = null;// the Camera 
	this.activeController = null;		
	this.DoomRadius = 128;
	this.ringOfDoom = null;	
	//------------------------------------------------
	this.sacrifices = 0;
	
	this.Intro = true; 
	this.PreCycles = 0;
	this.GameMode = true; 
	this.EndCycles = 0;
	this.EndMode = false;
	this.Win = false; 
	
	this.introScreen = null;
	this.endWinScreen = null;
	this.endLoseScreen = null;
	//--------- New Constants-------------------------
	this.backgroundColor = "black";
	//------------------------------------------------
	
	//----- Sets the Player ID so AI can tell who is the player regardless of position in Array
	this.SetPlayerID = function(id) 
	{
		this.playerID = id;
	}
	
	//----- MAIN SETUP FUNCTION// called on creation to set up the screen and any classes it contains.
	this.Setup = function()
	{		
		
		
		this.ringOfDoom = new RingOfDoom(this,new Vector(0,0),0, this.level.tx_necroRing1);
		this.level.spriteArray[this.level.spriteArray.length] = this.ringOfDoom;
		
		this.endWinScreen = new Sprite(this,new Vector(-8000,-8000),0, this.level.scrEndWin);
		this.level.spriteArray[this.level.spriteArray.length] = this.endWinScreen;
		
		this.introScreen = new Sprite(this,new Vector(),0, this.level.scrIntro);
		this.level.spriteArray[this.level.spriteArray.length] = this.introScreen;
		
		
		
		this.activeCamera.position.z = playerInput.CamZ;
		this.activeScene.add(this.activeCamera);
		
		this.ambientLight = new THREE.AmbientLight( 0xffffff);
		this.activeScene.add(this.ambientLight);

		var tmpX;
		var tmpY = 2048;
		var index = 0;
		
		for(var i = 0; i < 3; i++)
		{
			tmpX = -2048;
			for(var j = 0; j < 3; j++)
			{
				var bgMap = THREE.ImageUtils.loadTexture( this.level.backgroundImage.src[index] );
				var bgGeo = new THREE.PlaneGeometry(this.level.backgroundImage.width, this.level.backgroundImage.height);
				var bgMaterial = new THREE.MeshLambertMaterial( {map: bgMap } );
				var bgMesh = new THREE.Mesh(bgGeo, bgMaterial);
				bgMesh.sprite = this;
				bgMesh.position.set(tmpX, tmpY, -1)
				this.activeScene.add(bgMesh);
				tmpX += 2048;
				index += 1;
			}
			tmpY -= 2048;
			
		}
		
		this.cameraController = new CameraController(this);
		READY = true;
		

	}
	
	
	this.takeControlOfCharacter = function(_character)
	{
		if(this.activeController != null)
		{
			this.activeController.botModeActive = true;
		}
		
		this.SetPlayerID(_character.ID);
		this.activeController = _character.myController;
		this.activeController.botModeActive = false;
		this.activeController.indoctroJuice = this.activeController.indoctroJuiceMAX;
		
//		_character.setMap(this.level.peasentInd.src);
//		this.activeScene.remove(_character.mesh);
		_character.material.map = this.level.peasentPos.map;
		_character.map = _character.material.map;
		
		this.animation = new TextureAnimator(_character.map, 8, 2, 3, 1, 150 );
		_character.setAnimated(this.animation);
//		this.activeScene.add(_character.mesh);
		
	}	
	
	
	
	this.Update = function()
	{
		if(playerInput.mouseRight && this.Intro == false && this.GameMode == true )
		{
			this.sacrifices = 11;
		
			
		}
		
		
		
		
		if(this.PreCycles < 5 && this.Intro == true)
		{
			this.PreCycles++;
		}
		else if(this.PreCycles >= 5 && this.Intro == true)
		{
			
			this.GameMode = false; 
		}
		
		if(playerInput.mouseLeft && this.Intro == true && this.GameMode == false)
		{
			this.Intro = false; 
			this.GameMode = true; 
			this.activeScene.remove(this.introScreen.mesh);
		
		}
		
		if (this.sacrifices > 10)
		{
			this.Win = true; 
			
		}
		
		
		if (this.Win == true)
		{
			this.EndMode = true;
			this.endWinScreen.pos = this.cameraController.pos;
			
		}
		
		
		
		
		if(this.GameMode)
		{
			if(this.paused == false)
			{
				for (j in this.level.controllerArray) {
					controller = this.level.controllerArray[j]
					controller.update();
	
	
				}
			}
			
			
			for (i in this.level.spriteArray) 
			{
			
				s = this.level.spriteArray[i]
				if(this.paused == false)
				{
				  s.Update();
				}
				s.Drawself();
	
			}
			
			this.GameLogicUpdate();
			
			//----- Cam Update
			this.cameraController.updateCamera();
			this.activeCamera.position.x = this.cameraController.pos.x;
			this.activeCamera.position.y = this.cameraController.pos.y;
			this.activeCamera.position.z = playerInput.camZ;
			
			if(this.Intro)
			{
				this.introScreen.pos = this.cameraController.pos;
			}
			
			if(this.EndMode)
			{
				
				this.EndCycles ++;
			}
			if(this.EndMode && this.EndCycles > 100)
			{
				
				this.endWinScreen.pos = this.cameraController.pos;
			}
			if(this.EndMode && this.EndCycles > 120)
			{
				
				this.GameMode = false;
			}
			
			//console.log("CamZ" + playerInput.camZ);
		}
		
	}
	
	this.sacrifice = function()
	{
		if(this.activeController.character.pos.x > 120 && this.activeController.character.pos.x < 160 && this.activeController.character.pos.y < -2300)
		{
			this.activeController.character.explodeAnim(true);
		}
	}

	this.GameLogicUpdate = function()
	{
		if(playerInput.mouseLeft)
		{
			this.activeController.castIndoctrinate()
			
		}
		
		this.sacrifice();
		
	}
	
}
