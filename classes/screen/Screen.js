//--------------------------------------------------------------------------
//------------- Screen -----------------------------------------------------
//--------------------------------------------------------------------------
//-- Abstract Screen Base Class. All other GamesScreens and Menus extend from this class. 
function Screen(_width,_height)
{
	//-------- Initialized Vars ---------------------
	this.level = new Level(this);
	this.spawnCountID = 0; //DO NOT TOUCH. Modify only using the GenerateObjectID() to get a new object ID
	this.width = _width; //Screen width
	this.height = _height; //Screen height
	//------------------------------------------------
	
	
	// THREE.js
	
	this.activeCamera = new THREE.PerspectiveCamera( 70, _width / _height, 1, 1000 )
	this.activeScene = new THREE.Scene();
	this.ambientLight;
	


	
	// --------- Generate ID // Javascript does not have an 'Object Is Equal To' function so each object spawned has to be stamped with an ID"
	this.GenerateObjectID = function()
	{
		this.spawnCountID++;
		return this.spawnCountID;
	}
	
	
	//----- MAIN SETUP FUNCTION// called on creation to set up the screen and any classes it contains.
	this.Setup = function()
	{
		//create sprites to draw here
		
	}
	//----- MAIN UPDATE FUNCTION //called by _Main_ game loop.
	this.Update = function()
	{
//		this.clearCanvas();
		
		
		for (i in this.level.spriteArray) {
			s = this.level.spriteArray[i]
			s.Update();
			s.Drawself();

		}
		
	}
	
	//----- Clears the canvis for drawing
/*	this.clearCanvas = function()
	{
		
		context.fillStyle = this.level.backgroundColor;
		context.clearRect(0, 0, this.width, this.height);
	}
*/	
}