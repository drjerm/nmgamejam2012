String.prototype.contains = function(s) {
	return this.indexOf(s) > 0;
}
//--------------------------------------------------------------------------
//------------- LoadScreen -------------------------------------------------
//--------------------------------------------------------------------------
//-- Default LoadScreen 
function LoadScreen(width, height, level)
{
	Screen.call(this, width, height);
	//-------- Initialized Vars ---------------------
	this.level = level;
	//------------------------------------------------
	
	//--------- New Constants-------------------------
	this.backgroundColor = "red";
	//------------------------------------------------

	//This is where you make the screen look at pretty
	this.Setup = function() {
		this.activeCamera.position.z = 600;
		this.activeScene.add(this.activeCamera);

		this.ambientLight = new THREE.AmbientLight( 0xffffff);
		this.activeScene.add(this.ambientLight);

		var sphere = new THREE.SphereGeometry(300, 16, 16);
		var material = new THREE.MeshBasicMaterial({color: 0xCC0000});

		var mesh = new THREE.Mesh(sphere, material);
		mesh.position.set(0,0,10);

		this.activeScene.add(mesh);
	}

	//Refer to GameScreen -> this.Setup, and how this.bgMap is initialized.
	this.Load = function(callback) {

		var assets = this.level.assets;
		var imageloader = new THREE.ImageLoader();

		//[CHANGE THIS TO A FOREACH; That means change assets to a list of objects]
		for(var i = 0; i < assets.length; ++i) {
			imageloader.load( assets[i].src );
		}

		var loaded = 0;
		imageloader.addEventListener("load", function(event) {
			loaded++;
			var image = event.content;
			var texture = new THREE.Texture(image);
			texture.needsUpdate = true;

			//This is placing the texture with the correct
			//item in assets. It checks to see if the src
			//is similar.
			//[CHANGE THIS]
			for(var i = 0; i < assets.length; ++i) {
				if(image.src.contains(assets[i].src)) {
					assets[i].texture = texture;
					break;
				}
			}

			if(loaded == assets.length) {
				console.log(assets);
				console.log("loaded");
				imageloader.removeEventListener("load");
				return callback();
			}
		});
	}	
		
	
	this.Update = function()
	{

	}

	
	
}