//--------------------------------------------------------------------------
//------------- Animation ---------------------------------------------
//--------------------------------------------------------------------------

function TextureAnimator(texture, tilesHoriz, tilesVert, numRowTiles, tileRow, tileDispDuration) {
	// note: texture passed by reference, will be updated by the update
	// function.
	this.repeat = true;
	this.playing = true;
	
	this.row = tileRow;
	this.tileRow = tilesVert - this.row;
	this.tilesHorizontal = tilesHoriz;
	this.tilesVertical = tilesVert;
	// how many images does this spritesheet contain?
	// usually equals tilesHoriz * tilesVert, but not necessarily,
	// if there at blank tiles at the bottom of the spritesheet.
	this.numberOfTiles = numRowTiles;
	
	texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
	texture.repeat.set(1 / this.tilesHorizontal, 1 / this.tilesVertical);
	
//	this.offsetY = this.tileRow * this.tileSize;
//	texture.offset.y = this.offsetY;
	
	// how long should each image be displayed?
	this.tileDisplayDuration = tileDispDuration;
	
	
	// how long has the current image been displayed?
	this.currentDisplayTime = 0;
	// which image is currently being displayed?
	this.currentTile = 0;
	
	this.update = function(milliSec) {
		this.currentDisplayTime += milliSec;
		while (this.currentDisplayTime > this.tileDisplayDuration && this.playing) {
			this.currentDisplayTime -= this.tileDisplayDuration;
			this.currentTile++;
			if (this.currentTile == this.numberOfTiles)
				{
					if(this.repeat)
						this.currentTile = 0;
					else
						this.playing = false;
				}
			var currentColumn = this.currentTile % this.numberOfTiles;
			texture.offset.x = currentColumn / this.tilesHorizontal;
			var currentRow = this.tileRow;
			texture.offset.y = currentRow / this.tilesVertical;

		}
	};
	
	this.changeRow = function(row, numTiles){
		this.numberOfTiles = numTiles;
		this.row = row;
		this.tileRow = tilesVert - this.row;
	}
}

/*

//--------------------------------------------------------------------------
//------------- Animation ---------------------------------------------
//--------------------------------------------------------------------------

function TextureAnimator(texture, tilesHoriz, tilesVert, numTiles, tileDispDuration) {
	// note: texture passed by reference, will be updated by the update
	// function.
	this.tilesHorizontal = tilesHoriz;
	this.tilesVertical = tilesVert;
	// how many images does this spritesheet contain?
	// usually equals tilesHoriz * tilesVert, but not necessarily,
	// if there at blank tiles at the bottom of the spritesheet.
	this.numberOfTiles = numTiles;
	
	texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
	texture.repeat.set(1 / this.tilesHorizontal, 1 / this.tilesVertical);
	// how long should each image be displayed?
	this.tileDisplayDuration = tileDispDuration;
	
	
	// how long has the current image been displayed?
	this.currentDisplayTime = 0;
	// which image is currently being displayed?
	this.currentTile = 0;
	
	this.update = function(milliSec) {
		this.currentDisplayTime += milliSec;
		while (this.currentDisplayTime > this.tileDisplayDuration) {
			this.currentDisplayTime -= this.tileDisplayDuration;
			this.currentTile++;
			if (this.currentTile == this.numberOfTiles)
				this.currentTile = 0;
			
			var currentColumn = this.currentTile % this.tilesHorizontal;
			texture.offset.x = currentColumn / this.tilesHorizontal;
			var currentRow = Math
					.floor(this.currentTile / this.tilesHorizontal);
			texture.offset.y = currentRow / this.tilesVertical;
		}
	};
}

*/