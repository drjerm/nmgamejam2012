
/// !!! DOCUMENTATION !!! ///
/// http://www.schillmania.com/projects/soundmanager2/doc/

//document.write("<div id=\"sm2-container\"></div>");

var aURL = "/nmgamejam2012/classes/audio/"

soundManager.setup({
	flashVersion: 9,
	url: aURL,
	flashLoadTimeout: 0,
	useHighPerformance: true,
	defaultOptions: {
		autoPlay: false
	}
});

soundManager.onready(function() {
	soundManager.createSound({
		id: "bg_music",
		url: aURL+"data/mbg.mp3",
		autoPlay: true
	})
});

// screen offset of 1024

// distance ! replaced with distanceToPoint
//function auddist(x, y, px, py) {
//	return Math.sqrt(Math.pow(x - px) + Math.pow(y - py) )
//}

// box collision
//function audcolb(x, y, px, py, pw, ph) {
//	if( (px < x)
//       && (py < y)
//       && ((px + pw) > x)
//       && ((py + ph) > y) ) {
//           return true;
//    }
//	return false;
//}

// sphere collision
function audcols(x, y, px, py, r, pr) {
	if(Math.sqrt(Math.pow(x - px,2) + Math.pow(y - py,2)) < (r + pr)) {
		return true;
	}
	return false;
}
function carte(x, y) {
	var r = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
	var deg = Math.pow(Math.tan(y / x), -1);
	return Vector(deg, r);
}
function polar(r, deg) {
		var x = r * Math.cos(deg);
		var y = r * Math.sin(deg);
		return Vector(x, y);
}

function Sound(idx, srcx, _pos) {
	this.el = soundManager.createSound({
		id: idx,
		url: srcx,
		volume: 0,
	});
	this.position = new Vector(_pos.x,_pos.y);
	this.r = 200;
	this.scalar = (100 / this.r);
	this.dist;
	this.Play = function() {
		if(audcols(gameScreen.activeCamera.position.x, gameScreen.activeCamera.position.y, this.position.x, this.position.y, window.innerWidth, this.r)) {
			this.dist = distanceToPoint(this.position.x, this.position.y, gameScreen.activeCamera.position.x, gameScreen.activeCamera.position.y);
			if(!this.el.playState)
				this.el.play();
			var temp = this.dist * this.scalar * 2;
			if(temp > 100) temp = 100;
			this.el.setVolume(temp);
		}
	}
	this.Stop = function() {
		if(audcols(gameScreen.activeCamera.position.x, gameScreen.activeCamera.position.y, this.position.x, this.position.y, window.innerWidth, this.r)) {
			this.dist = distanceToPoint(this.position.x, this.position.y, gameScreen.activeCamera.position.x, gameScreen.activeCamera.position.y);
			if(this.el.playState)
				this.el.stop();
			var temp = this.dist * this.scalar * 2;
			if(temp > 100) temp = 100;
			this.el.setVolume(temp);
		}
	}
}

var A_Good = new Sound("s_good", aURL+"data/good.mp3", 0, 0);
var A_Bad = new Sound("s_good", aURL+"data/bad.mp3", 0, 0);
var A_Sel = new Sound("s_sel", aURL+"data/sel.mp3", 0, 0);
var A_End = new Sound("s_end", aURL+"data/end.mp3", 0, 0);

function A_Guard(_pos) {
	this.position = new Vector(_pos.x,_pos.y);
	this.type = Math.floor(Math.random()*2);
	/// "Hey!"
	this.Hey = new Sound("s_g"+type+"hey"+vr, aURL+"data/g"+type+"hey.mp3", _pos);
	/// "What was that."
	this.What = new Sound("s_g"+type+"wht"+vr, aURL+"data/g"+type+"wht.mp3", _pos);
	/// "Get Back Here!"
	this.Gethere = new Sound("s_g"+type+"gbh"+vr, aURL+"data/g"+type+"gbh.mp3", _pos);
	/// "What you doing?"
	this.Doing = new Sound("s_g"+type+"d"+vr, aURL+"data/g"+type+"d.mp3", _pos);
	
	this.Steps = new Sound("s_gstp", aURL+"data/stone.mp3", _pos);
}

function A_Priest(_pos) {
	this.position = new Vector(_pos.x,_pos.y);
	this.type = Math.floor(Math.random()*5);
	this.Indoc = new Sound("s_p"+type+"idc"+vr, aURL+"data/p"+type+"idc.mp3", _pos);
	this.Steps = new Sound("s_pstp", aURL+"data/dirt.mp3", _pos);
}
