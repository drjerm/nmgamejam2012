//------- Utilities ----------

//Takes two points and returns the rotation of one to the other. 
function rotateToPoint(x1,y1,x2,y2)
{
    var rotation;
    
	if( (x2-x1) > 0)
	{
		rotation = Math.atan((y2-y1)/(x2-x1));
		return rotation;
	}
	
   	else if ((x2-x1) == 0)
	{
		if ((y2-y1) > 0)
		{
			rotation = (Math.PI/2);
			return rotation;
		}
		else
		{
			rotation = -(Math.PI/2);
			return rotation;
		}
	}
	
	else
	{
		rotation = Math.atan((y2-y1)/(x2-x1)) + Math.PI;
		return rotation;
	}
	
}

//----- uses pythagerean to return a distance between two points. (there is a line from me to you)
function distanceToPoint (x1,y1,x2,y2) {
	return Math.sqrt(Math.pow((x2-x1),2) + Math.pow((y2-y1),2));
}


//----- Takes a rotation and returns it in the form of 0<r<2PI
function simplifyRotation(rotation)
{
	if(rotation > Math.PI*2)
	{
		rotation -= Math.PI*2;
	}
	else if(rotation <= 0)
	{
		rotation += Math.PI*2;
	}
	
	return rotation;

}

//----- given Angle A and B's directions and A's magnitude, returns the projection of A onto B
function dotVectors(angleA,angleB,magnitudeA)
{
	//this.MagnitudeAb = magnitudeA*Math.cos(angleA-angleB);
}


/**
 * Returns a random number from -1 to 1.  Statistically, this
 * function is likely to return numbers very close to zero.
 */
function randomBinomial() {
    return Math.random() - Math.random();    
}


/**
 * Creates an n-dimensional square array, with a default value
 * @param {Number} defaultValue the default value to assign to the entire array
 * @param {Number} the dimension of the square array
 * 
 * Examples:
 *  createArray(2) - creates an array of length 2
 *  createArray(2,3) - creates a 2-dimensional array of size 2x3
 *  createArray(3,3,3) - creates a 3-dimensional array of size 3x3x3
 */
function createSquareArray(dimension) 
{
    var a = new Array(dimension);
    
    for(var i = 0; i < dimension; i++) {
        a[i] = new Array(dimension);
        
        for(var j = 0; j < dimension; j++) 
        {
            a[i][j] = 0;
        }
    }
    
    return a;
}













