//--------------------------------------------------------------------------
//------------- Player -----------------------------------------------------
//--------------------------------------------------------------------------

function Player(_screen, _pos,r,img)
{	Character.call(this,_screen, _pos,r,img);
	
	//_screen.SetPlayerID(this.ID)//sets the player ID so AI's know who is the player. 
	
	//--------- New Constants-------------------------
	this.maxSpeed = 10;
	this.angleSpinDivisor = 5;
	this.type = 2;
//	this.playerLight = new THREE.PointLight( 0xffffff, 1, 100 );
//	_screen.activeScene.add(this.playerLight);
	//------------------------------------------------
	
	//--------- MAIN UPDATE FUNCTION 
	//<<< OVERIDE >>>
	this.Update = function()
	{

			this.UpdateCharacter();
			this.UpdateCollidableSprite();
			this.UpdatePlayer();

		
	}
	this.UpdatePlayer = function()
	{
//		this.playerLight.position.x = this.pos.x;
//		this.playerLight.position.y = this.pos.y;
//		this.playerLight.position.z = 10;
		this.rot = this.rot - Math.PI/2;
//		console.log(this.playerLight.position.x + "," + this.playerLight.position.y);
		//TODO:Things like Health regeneration and specific Player functions here. Anything regarding Player input should go in the PlayerController
	
	
	}
	
	this.movingAnim = function(bool)
	{
		if(bool)
		{
			if(this.animation.row != 2)
				this.animation.changeRow(2, 8);
		} else {
			this.animation.changeRow(1,3);
		}
	}
	
	this.fleeingAnim = function(bool)
	{
		if(bool)
		{
			if(this.animation.row != 3)
				this.animation.changeRow(3, 8);
		} else {
			this.animation.changeRow(1,3);
		}
	}	
		
	this.possessingAnim = function(bool)
	{
		if(bool)
		{
			if(this.animation.row != 4)
				this.animation.changeRow(4, 6);
		} else {
			this.animation.changeRow(1,3);
		}
	}
	
	this.explodeAnim = function(bool)
	{
		this.material.map = activeScreen.level.peasentExp.map;
		this.map = this.material.map;
		
		this.animation = new TextureAnimator(this.map, 8, 1, 8, 1, 150 );
		this.setAnimated(this.animation);
		this.animation.repeat = false;
	}

}