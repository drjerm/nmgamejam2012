//--------------------------------------------------------------------------
//------------- Bot --------------------------------------------------------
//--------------------------------------------------------------------------
function Bot(_screen, _pos,r,img)
{	Character.call(this,_screen, _pos,r,img);
	
	//--------- New Constants-------------------------
	this.maxSpeed = 3;
	this.type = 1; 
	
	
	//------------------------------------------------

	//--------- MAIN UPDATE FUNCTION 
	//<<< OVERIDE >>>
	this.Update = function()
	{
		this.UpdateCharacter();
		this.UpdateCollidableSprite();
		this.UpdateBot();
	}
	
	this.UpdateBot = function()
	{
		this.rot = this.rot + Math.PI/2;
		//TODO:Things like Health regeneration and specific Bot functions here. Anything regarding Bot AI, decision making and states should go in Bot Controller
	}
	
	this.movingAnim = function(bool)
	{
		if(bool)
		{
			if(this.animation.row != 3)
				this.animation.changeRow(3, 8);
		} else {
			this.animation.changeRow(1,1);
		}
		
	}
	
	this.runningAnim = function(bool)
	{
		if(bool)
		{
			if( this.animation.row!= 4)
				this.animation.changeRow(4, 8);
		} else
			this.animation.changeRow(1,1);
	}
	
	this.alertAnim = function(bool)
	{
		if(bool)
		{
			if (this.animation.row != 2)
				this.animation.changeRow(2,5);
			
			else
			{
				if(this.animation.currentTile == 4)
					this.animation.changeRow(1,1)
			}
		} else
			this.animation.changeRow(1,1);
	}
	
	this.attackAnim = function(bool)
	{
		if(bool)
		{
			if (this.animation.row != 5)
				this.animation.changeRow(5,7);
			
			else
			{
				if(this.animation.currentTile == 5)
					this.animation.changeRow(4,8);
			}
		} else
			this.animation.changeRow(1,1);
	}
}