//--------------------------------------------------------------------------
//------------- Character --------------------------------------------------
//--------------------------------------------------------------------------

function Character(_screen, _pos,r,img)
{ 	Collider.call(this,_screen, _pos,r,img);
	
	_screen.level.characterArray[_screen.level.characterArray.length] = this; //Adds Itself to the sceen's character Array for character updates. 

	//-------- Initialized Vars ---------------------
	this.Health = 100;
	this.MaxHealth = 100;
	this.IsAlive = true;
	
	this.myController = null; 
	
	this.height = 64;
	this.width  = 64;
	
	//------ OVERIDEDED!!!!!!!-----------
	//------------------------------------------------
	this.top    = this.pos.y + this.height/2;
	this.bottom = this.pos.y - this.height/2;
	this.right  = this.pos.x + this.width/2;
	this.left   = this.pos.x - this.width/2;
	this.mesh.rotation.z = this.mesh.rotation;
	//------------------------------------------------
	
	//--------- New Constants-------------------------
	this.usesRadColision = true; //Characters use radial collision because they do not have defined shapes. 
	this.static = false; //Characters move and can receive forces from collisions
	//------------------------------------------------
	
	//--------- MAIN UPDATE FUNCTION 
	//<<< OVERIDE >>>
	this.Update = function()
	{
		
		
		this.UpdateCharacter();
		this.UpdateCollidableSprite();
		
	}
	
	//----- Set the controller  for the character. 
	this.setMyController = function(_controller)
	{
		this.myController = _controller;
		
	}
	
	
	this.UpdateCharacter = function()
	{
		
		//TODO: stuff like life regen, shield recharge or what ever go here. 
	
	}
	//------ OVERIDEDED!!!!!!!-----------
	this.UpdateCollisionBox = function()
	{
		//---- Update corner vertexes for collision
		this.top    = this.pos.y + this.height/2;
		this.bottom = this.pos.y - this.height/2;
		this.right  = this.pos.x + this.width/2;
		this.left   = this.pos.x - this.width/2;
	}
	
	
	//need to revise this	
	this.CheckWorldCollision = function()
	{	
		if (this.pos.x <= 0) this.pos.x = 0;
  		if ((this.pos.x + this.width) >= WIDTH) this.pos.x = WIDTH - this.width;
 		if (this.pos.y <= 0) this.pos.y = 0;
  		if ((this.pos.y + this.height) >= HEIGHT) this.pos.y = HEIGHT - this.height;
	
	}
}