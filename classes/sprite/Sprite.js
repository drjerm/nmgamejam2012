//--------------------------------------------------------------------------
//------------- Sprite ---------------------------------------------
//--------------------------------------------------------------------------
function Sprite(_screen, _pos,r,_img)
{	Entity.call(this,_screen,_pos,r);
	
	//-------- Initialized Vars ---------------------
	 
	
	this.img = _img;
	
	this.height = this.img.height;
	this.width  = this.img.width;
	
	this.center_h = this.height/2;
	this.center_w = this.width/2;
	
	this.animated = false;
	
	this.animation;
	//------------------------------------------------
	
	// Three.js Initializing

	this.map = this.img.map;
	this.geo = new THREE.PlaneGeometry(this.width, this.height);
	this.material = new THREE.MeshLambertMaterial({map: this.map});
	this.material.transparent = true;
	this.mesh = new THREE.Mesh(this.geo, this.material);
	this.mesh.sprite = this;
	_screen.activeScene.add(this.mesh);
	
	//------------------------------------------------

	this.setAnimated = function(inAnim)
	{
		this.animated = true;
		
		this.animation = inAnim;
	}
	
	this.setMap = function(inMap)
	{
		this.map = inMap;
		this.material.map = this.map;
		this.material.needsUpdate;
		
		
		
	}
	
	//--------- Draw Self 
	this.Drawself = function()
	{
		if(this.animated)
			{
				this.animation.update(delta*1000);
			
			}
		this.mesh.position.x = this.pos.x;	
		this.mesh.position.y = this.pos.y;
		this.mesh.rotation.z = this.rot;
		
	}
	
	

	//--------- MAIN UPDATE FUNCTION 
	//<<< OVERIDE >>>
	this.Update = function()
	{
		
	
	}

}
