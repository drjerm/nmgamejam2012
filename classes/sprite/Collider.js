//--------------------------------------------------------------------------
//------------- Collider ---------------------------------------------------
//--------------------------------------------------------------------------
function Collider(_screen, _pos,r,img)
{	Sprite.call(this,_screen, _pos,r,img);
	
	_screen.level.colliderArray[_screen.level.colliderArray.length] = this; //Adds Itself to the sceen's collidable Array for collision checking
	
	//-------- Initialized Vars ---------------------
	this.doesCollide = true; //false means no clip
	this.static = true;// Static objects cannot move or receive force from impacts from non-static objects. Solid walls for example
	this.usesRadColision = false; //object defaults to box collision unless set to true	
	this.isMoving = false; //moving this frame. Ignored if object is static. It never moves
	this.bounceConstant = 0; //A value of 1 is a perfect elastic collision, speed is maintained
	
	this.radialWidth =  distanceToPoint(0,0,this.center_w,this.center_h);
	
	this.speed = 0;
	this.maxSpeed = 0;
	
	this.vectorAngle = 0;
	
	this.speedVector = new Vector(0,0);
	
	this.acceleration = 0.1;
	this.decceleration = 0.2;
	//------------------------------------------------
	
	//-------------- Temp Vars for Collision----------
	this.TimeDelta = 1; //If less than 1 object collided and did not get to move the full distance. Multiplied to the speed vectors in MoveAlongVector()
	this.collisionNormalAngle = 0;
	this.collisionAngle = 0;
	//-- collision box vertexes
	this.top    = this.pos.y + this.center_h;
	this.bottom = this.pos.y - this.center_h;
	this.right  = this.pos.x + this.center_w;
	this.left   = this.pos.x - this.center_w;
	//this.tempDisc = 0;
	//this.tempAngle = 0; //for use in rotation calculations
	//------------------------------------------------
	
	//--------- MAIN UPDATE FUNCTION 
	//<<< OVERIDE >>>
	this.Update = function()
	{
		this.UpdateCollidableSprite();
	}
	
	
	
	this.UpdateCollidableSprite = function()
	{
		if(this.static == false)
		{
			
			this.UpdateSpeed();
			this.CheckCollision();
			this.MoveAlongVector();
		}
	}
	
	//------ Updates motion vectors based on if the object is moving or not
	this.UpdateSpeed = function()
	{
		//if is accelerating, speed up down
		if (this.isMoving) 
		{
			this.speed += this.acceleration;

		}
		//if is stopping, slow down
		else 
		{ 
			this.speed -= this.decceleration;
		}
	
		//check speed bounds	
		if(this.speed > this.maxSpeed)
		{
			this.speed = this.maxSpeed;
		}
		else if (this.speed < 0)
		{
			this.speed  = 0;	
		}
		
		//simplify all rotations to stay between 0 and 2pi
		this.rot = simplifyRotation(this.rot);
		this.vectorAngle = simplifyRotation(this.vectorAngle);
		
		
		//get my final vector components 		
		this.speedVector.x = Math.cos(this.vectorAngle)*this.speed;
		this.speedVector.y = Math.sin(this.vectorAngle)*this.speed;
	
	}
	
	//------ Checks to see if this this CollidableSprite is colliding with another CollidableSprite and then prevents intersection. 
	this.CheckCollision = function()
	{
		for (var i in _screen.level.colliderArray) 
		{
	 		var c = _screen.level.colliderArray[i];
			if( this.ID != c.ID && this.doesCollide == true && c.doesCollide == true) //Not me and I collide and the object I am checking collides
			{
				//this.CheckRadialCollision(c);
				this.CheckBoxCollision(c);
			}
			
		}
	}
	
	
	
	//------ Checks Collision based on Radial math and then applies apropriate forces. 
	
	this.CheckBoxCollision = function(c)
	{
		
		
		if((this.left > c.left && this.left < c.right) || (this.right < c.right && this.right > c.left ))
		{
			//Collides with Top
			if(this.bottom < c.top && this.top > c.top)
			{
				
				if(this.speedVector.y < 0)
				{
					this.speedVector.y = 0;
				}
			}//Collides with Bottom
			else if(this.top > c.bottom && this.bottom < c.bottom)
			{
				if(this.speedVector.y > 0)
				{
					this.speedVector.y = 0;
				}
				
			}		
		}
		
		if((this.bottom > c.bottom && this.bottom < c.top) || (this.top < c.top && this.top > c.bottom ))
		{
			//Collides with Top
			if(this.left < c.right && this.right > c.right)
			{
				
				if(this.speedVector.x < 0)
				{
					this.speedVector.x = 0;
				}
			}//Collides with Bottom
			else if(this.right > c.left && this.left < c.left)
			{
				if(this.speedVector.x > 0)
				{
					this.speedVector.x = 0;
				}
				
			}		
		}
	
	
	}
	
	
	
	this.CheckRadialCollision = function(c)
	{
		//if (  DistanceToPoint (c.pos.x,c.pos.y,this.pos.x,this.pos.y) < this.radialWidth + c.radialWidth)
		if ( (Math.pow((c.pos.x-this.pos.x),2) + Math.pow((c.pos.y-this.pos.y),2)) <  Math.pow((this.radialWidth + c.getRadius()),2) )
		{
			this.TimeDelta = 1; //Does nothing but stop the object permanently for now. 
			
			this.collisionNormalAngle = c.getNormalAngle(rotateToPoint(this.pos.x,this.pos.y,c.pos.x,c.pos.y));
			//this.speed = dotVectors(this.vectorAngle,this.collisionNormalAngle,this.speed);
			this.speedVector.x = Math.cos(this.collisionNormalAngle)*this.speed;
			this.speedVector.y = Math.sin(this.collisionNormalAngle)*this.speed;
		}
		
		
	}
	
	this.RotateCollisionBox = function()
	{
		var b = this.pos.y;
		this.pos.y = this.pos.x;
		this.pos.x = b;
		
		this.center_h = this.pos.y-this.height/2;
		this.center_w = this.pos.x-this.width/2;
		
	this.UpdateCollisionBox();
	}
	
	
	this.UpdateCollisionBox = function()
	{
		//---- Update corner vertexes for collision
		this.top    = this.pos.y + this.center_h;
		this.bottom = this.pos.y - this.center_h;
		this.right  = this.pos.x + this.center_w;
		this.left   = this.pos.x - this.center_w;
	
		
	}
	
	//----- updates the position based on the movement vector components
	this.MoveAlongVector = function()
	{
		this.pos.x += this.speedVector.x*this.TimeDelta;
		this.pos.y += this.speedVector.y*this.TimeDelta;
		
		this.UpdateCollisionBox(); 
		
	}
	
	//----- Tests to see weather or not given point is within collision box
	this.checkPointInCollisionBox = function(point)
	{
		if (this.left < point.x && point.x < this.right && this.bottom < point.y && point.y < this.top)
		{
			return true; 
		}
		else 
		{
			return false;
		}
		
		
	}
	
	
	
	//----- returns radius of this object at the specified angle. 
	this.getRadius = function(angle)
	{
		if (this.usesRadColision = true)
		{
			return this.radialWidth; //for radial collision its just the radius of the circle;
		}
		else
		{
			//TODO: Box radius at angle. 
		}
	
	}
	//----- returns the normal of this object at the point of contact 
	this.getNormalAngle = function(angle)
	{
		if (this.usesRadColision = true)
		{
			angle += Math.PI/2; //for radial collision its just the tangent to the circle;
			return angle; 
		}
		else
		{
			//TODO: Box normal at angle. 
		}
	
	}
}