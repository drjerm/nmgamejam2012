//--------------ENGINE----------------------------
var container;
var context;


var DEBUG = true; // Debug mode (show FPS)
var EDITOR = false;
var READY = false;



var playerInput; //Contains the Player input class

var activeScreen;

var gameScreen;


//-------THREE.js variables ---------//

var clock;
var delta;
var elapsedTime;

var renderer;

//------- Initialize
function init() {

	container = document.getElementById( 'container' );
	
	renderer = new THREE.WebGLRenderer( { clearColor: 0x000000, clearAlpha: 1 } );
	
	WIDTH = window.innerWidth;
	HEIGHT = window.innerHeight;
	
	clock  = new THREE.Clock();
	renderer.setSize( window.innerWidth, window.innerHeight );
	container.appendChild( renderer.domElement ); 
	
	
	playerInput = new Input(); //Contains the Player input class

	//initialize game screens. 
	
	gameScreen = new GameScreen(WIDTH,HEIGHT);
	
	activeScreen = gameScreen;
	
    window.requestAnimFrame = (function(callback){
        return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function(callback){
            window.setTimeout(callback, 1000 / 60);
        };
    })();
    
    window.addEventListener( 'resize', onWindowResize, false );
	
    
    if(DEBUG){
    	container = document.getElementById( 'container' );
    	stats = new Stats();
    	stats.domElement.style.position = 'absolute';
    	stats.domElement.style.top = '0px';
    	container.appendChild( stats.domElement );
    }
    
	UpdateGame();
	
	
	
	
}


// -------- MAIN GAME LOOP
function UpdateGame() 
{

	
	if(READY)
	{
		delta = clock.getDelta();
		elapsedTime = clock.getElapsedTime();
		playerInput.Update();
		activeScreen.Update();
		renderer.render(activeScreen.activeScene, activeScreen.activeCamera);
		
		if(DEBUG){
			stats.update();
		}
		if(EDITOR){
		  EditorUpdate();
		}
	}
	requestAnimFrame(UpdateGame);
}

function onWindowResize() {

	WIDTH = window.innerWidth;
	HEIGHT = window.innerHeight;
	
	activeScreen.activeCamera.aspect = WIDTH / HEIGHT;
	activeScreen.activeCamera.updateProjectionMatrix();

	renderer.setSize( WIDTH, HEIGHT );

}





