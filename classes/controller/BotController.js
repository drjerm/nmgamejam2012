//--------------------------------------------------------------------------
//------------- Bot Controller ---------------------------------------------
//--------------------------------------------------------------------------
function BotController(_screen) // Default controller
{	Controller.call(this, _screen);
	
	//Wander-behavior related variables
	this.maxWanderRotation = 15;
	
	//Chase-behavior related variables
	this.moveVertical = true;
	
	//Declare AI states here
	this.state = 2; //default state: wander
	
	
	//--------- MAIN UPDATE FUNCTION
	//<<< OVERIDE >>>
	this.update = function()
	{
		this.updateController();
		this.updateBotController();
	}
	
	this.updateBotController = function() {
		this.updateAI();
	}
	
	//--------- Main Update AI function
	this.updateAI = function()
	{
		if(this.character.speed > .5)
		{
			
		}
		
	    //TODO:  THIS IS JUST TO TEST DIVERSE AI INPUTS - PLEASE DELETE ME LATER
	    if(playerInput.sKey) {
	        //alert("Seek Behavior Active");
	        this.state = 0;
	    }
	    
	    if(playerInput.fKey) {
	        //alert("Flee Behavior Active");
	        this.state = 1;
	    }
	    
	    if(playerInput.wKey) {
	       // alert("Wander Behavior Active");
	        this.state = 2;
	    }
	    
	    if(playerInput.aKey) {
	        this.state = 3;
	    }
	    
	    
	    if(this.state === 0) {
	    	this.seekTarget(_screen.playerID);
	    }
	            
	    else if(this.state == 1) {
	        this.fleeTarget(_screen.playerID);
	    }
	            
	    else if(this.state == 2) {
	        this.wander();
	    }
	    
	    else if(this.state == 3) 
	    {
	       this.apath;
	       this.waypointEntity;
	       this.counter = 0;
	        
	       if(this.counter > 120) {
	           this.counter -= 120;
	           this.apath = go(this.character.pos, _screen.activeController.character.pos, _screen.level.world); 
	       }
	       
	       else {
	           this.counter++;
	       }
	       
	       this.waypointEntity = goNext(this.apath, _screen.level.world);
            
           if(this.waypointEntity != null) {
               this.seekTarget(this.waypointEntity.ID);
           }
	     
	    }
	    
	    this.checkCharacterProximity(_screen.playerID);
// 	    
	    // else if(this.state == 4) {
	       // this.chaseTarget(_screen.playerID);
	    // }
// 	    
		
	}
	
	

	
	this.checkCharacterProximity = function(targetObjectID) 
	{
	    var proximity;
	    
	    for(i in _screen.level.spriteArray) //Should be CharacterArray or CollidableArray. 
        {
            var sprite = _screen.level.spriteArray[i];
            if (sprite.ID == targetObjectID) 
            {
                proximity = distanceToPoint(this.character.pos.x, this.character.pos.y, sprite.pos.x, sprite.pos.y);
                
                if(proximity <= 500)
                {
                    this.state = 0; //seek!        
                }
            
            
                if(proximity > 500) 
                {
                    this.state = 2; //back to wander    
                } 
            }
        }
	}
	
	
	


    /**
     * Seek the target given by the parameter objectID.
     * @param targetObjectID the objectID of the target to seek.
     * @author recardona
     */
	this.seekTarget = function(targetObjectID)
	{
		this.character.isMoving = true;
		
		if(this.character instanceof Bot)
			this.character.runningAnim(true);
		
		for(i in _screen.level.spriteArray) //Should be CharacterArray or CollidableArray. 
		{
	 		var sprite = _screen.level.spriteArray[i];
			if (sprite.ID === targetObjectID)
			{
				this.vectorAngleGoal = rotateToPoint(this.character.pos.x, this.character.pos.y, sprite.pos.x, sprite.pos.y);
				this.character.rot   = this.vectorAngleGoal;
			}
		}
	}
	
	
	/**
	 * Flee from the target given by the parameter objectID.
	 * @param targetObjectID the objectID of the target to flee from.
	 * @author recardona
	 */
	this.fleeTarget = function(targetObjectID)
	{
	    this.character.isMoving = true;
	    
		if(this.character instanceof Player)
			this.character.fleeingAnim(true);
	    
	    for(i in _screen.level.spriteArray)
	    {
	        var sprite = _screen.level.spriteArray[i];
	        if(sprite.ID === targetObjectID)
	        {
	            this.vectorAngleGoal = -rotateToPoint(this.character.pos.x, this.character.pos.y, sprite.pos.x, sprite.pos.y);
	            this.character.rot   = this.vectorAngleGoal;
	        }
	    }
	}
	
	
	this.chaseTarget = function(targetObjectID)
	{
	    this.character.isMoving = true;
	    
	    for(i in _screen.level.spriteArray)
	    {
	        var sprite = _screen.level.spriteArray[i];
	        if(sprite.ID === targetObjectID)
	        {
	           var distToTarget = distanceToPoint(sprite.pos.x,sprite.pos.y,this.character.pos.x,this.character.pos.y);
	           
	           var horizDist = (this.character.pos.x - sprite.pos.x); //if >0, I am to the right of my target (go left)
	           var vertDist  = (this.character.pos.y - sprite.pos.y); //If >0, I am above my target (go down)
               
               if(Math.abs(horizDist) == distToTarget || Math.abs(vertDist) == distToTarget)
               {//target is in direct horiz or vert line
                   this.seekTarget(targetObjectID);
               }
               
//                
//                
//                
//                
//                
	           // if(horizOffset != 0) {
	               // horizOffset /= Math.abs(horizOffset);
	               // this.vectorAngleGoal = Math.PI * horizOffset;
	               // this.character.rot = this.vectorAngleGoal;
	           // }
// 	           
	           // else if(vertOffset != 0) {
	               // vertOffset /= Math.abs(vertOffset);
	               // this.vectorAngleGoal = (Math.PI/2) * vertOffset;
	               // this.character.rot = this.vectorAngleGoal;
	           // }
	           
	        }
	        
	    }
	    
	}
	
	
	
	/**
	 * Wander in a random direction.
	 */
    this.wander = function()
    {
        //if it is not moving,
        if(!this.character.isMoving) {
            this.character.isMoving = true; //make it move
        }
        
        if(this.character instanceof Player)
        	this.character.movingAnim(true);
        
        var binomial = randomBinomial();
        
        //get the new vector angle by updating the old vector angle with a binomial multiple of the maximum rotation
        var newVectorAngle = this.character.vectorAngle + (binomial * this.maxWanderRotation);
        
        //get the new vector angle's components and move to that point
        //the factor 10 is completely arbitrary
        var newVectorAngleXComponent = 10*Math.cos(newVectorAngle) + this.character.pos.x;
        var newVectorAngleYComponent = 10*Math.sin(newVectorAngle) + this.character.pos.y;
        
        //the goal should be the new point to rotate to...
        this.vectorAngleGoal = rotateToPoint(this.character.pos.x, this.character.pos.y, newVectorAngleXComponent, newVectorAngleYComponent);
        
        //..but the image rotation should match the direction it is actually going. 
        this.character.rot = this.character.vectorAngle;
    }
	
}