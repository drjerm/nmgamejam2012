//--------------------------------------------------------------------------
//------------- Controller -------------------------------------------------
//--------------------------------------------------------------------------
function Controller(_screen) // Default controller
{
	//alert("Controller");
	
	//-------- Initialized Vars ---------------------
	this.character = null
	
	this.vectorAngleGoal = 0; //Angle character want to travel Set by AI or Player input 
	this.angleSpinDivisor = 20; //Controls how fast character can spin to meet its goal angle
	//------------------------------------------------

	
	//--------- MAIN UPDATE FUNCTION //calls parent update
	//<<< OVERIDE >>>
	this.update = function()
	{
		this.updateController();
	}
	
	this.updateController = function() {
		this.updateAngleFromGoal();
	}
	
	//----- Set the character for the controller. 
	this.setCharacter = function(_character)
	{
		this.character = _character;
		this.character.setMyController(this);
		
	}
	
	//------------- Updates Characters vector angle based on their goal angle. Keeps the from instantly snapping to face targets
	this.updateAngleFromGoal = function()
	{
		//Simplify vectorAngleGoal between 0 and 360 degrees
		this.vectorAngleGoal = simplifyRotation(this.vectorAngleGoal);
		
		
		//Adjusts vector angle to approach the vector angle goal 
		if (this.vectorAngleGoal >= 0 && this.character.vectorAngle >  this.vectorAngleGoal + Math.PI)
		{
			this.vectorAngleGoal += Math.PI*2;
		}
		else if (this.vectorAngleGoal <= Math.PI*2 && this.character.vectorAngle <  this.vectorAngleGoal - Math.PI)
		{
			this.vectorAngleGoal -= Math.PI*2;
		}
		this.character.vectorAngle -= (this.character.vectorAngle - this.vectorAngleGoal)/ this.angleSpinDivisor
	
	
	}
	
}