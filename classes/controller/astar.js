/**
 * A Path representation for A*.
 * Typically, f(x) = g(x) + h(x) is used to determine which
 * path to select, where:
 *  - g(x): the cost accumulated thus far in the path
 *  - h(x): the heuristically estimated distance to the goal
 *  - f(x): the path-finding evaluation function
 */
function Path(pathNode)
{
    this.path = pathNode;
    this.g = 0; // g(x)
    this.h = 0; // h(x)
    
    /**
     * Returns the total cost of this path thus far.
     * Typically, f(x) = g(x) + h(x)
     */
    this.f = function() {
        return (this.g + this.h);
    }
    
    /**
     * Returns the end of this path.
     */
    this.pathEnd = function() {
        return this.path[this.path.length-1];
    }
    
    /**
     * This function is used to sort paths according
     * to the smallest amount of cost incurred.
     */
    this.isCheaperThan = function(otherPath) {
        return (this.f() < otherPath.f());
    }
    
}



/**
 * Returns the next move for the path "found".
 */
function goNext(found, nodeMap)
{
    
    if(found == null)
        return null;
    
    else if(found.length === 1) //you're there!  stay put!
        return null;
        
    else
    {
        var frontNode = found.path.pop();
        return nodeMap.getNodeValueForNodeCoordinates(frontNode.x,frontNode.y); //this is an Entity, with an Object ID
    }
        
}




/**
 * Returns the node if it is in the parameter pathList, null otherwise.
 * @param {Path} a Path list which contains a Node of interest at the tail.
 * @param {Array<Path>} a Path list to search through
 */
function inPathList(node, pathList)
{
    for(pathIndex in pathList) 
    {
        if(pathList[pathIndex].length > 1)        
        {
            if(node.pathEnd() == pathList[pathIndex].pathEnd()) 
            {
                return pathList[pathIndex];
            }
        }
    }
    
    return null;
}



/**
 * Return the adjacent Nodes, given a Node in the NodeMap.
 * For simplicity, we only look at the cardinal directions. (N,S,E,W)
 * @param {Vector} pathNode a Node to look at
 * @param {NodeMap} nodeMap a NodeMap with which to determine adjacency
 * @return all nodes adjacent to pathNode in the nodeMap
 */
function adjacency(pathNode, nodeMap)
{
    var nodeXCoord = pathNode.x;
    var nodeYCoord = pathNode.y;
    var adjacentNodes = new Array();
   
    
   
   
    //Check N
    if(nodeMap.getNodeValueForNodeCoordinates(nodeXCoord, nodeYCoord+1) === 0)
    {   //need to check if we hit the dummy rows/columns to skip over them.
       
        if(nodeMap.getNodeValueForNodeCoordinates(nodeXCoord,nodeYCoord+2) !== null) {
            adjacentNodes[adjacentNodes.length] = new Vector(nodeXCoord,nodeYCoord+2);
        }
    }
    
    else if(nodeMap.getNodeValueForNodeCoordinates(nodeXCoord, nodeYCoord+1) !== null)
        adjacentNodes[adjacentNodes.length] = new Vector(nodeXCoord,nodeYCoord+1);
    
    
        
    //Check S
    if(nodeMap.getNodeValueForNodeCoordinates(nodeXCoord, nodeYCoord-1) === 0)
    {
        if(nodeMap.getNodeValueForNodeCoordinates(nodeXCoord,nodeYCoord-2) !== null) {
            adjacentNodes[adjacentNodes.length] = new Vector(nodeXCoord,nodeYCoord-2);
        }
    }
    
    else if(nodeMap.getNodeValueForNodeCoordinates(nodeXCoord, nodeYCoord-1) !== null)
        adjacentNodes[adjacentNodes.length] = new Vector(nodeXCoord,nodeYCoord-1);
    
    
    
    //Check W
    if(nodeMap.getNodeValueForNodeCoordinates(nodeXCoord-1, nodeYCoord) === 0)
    {
        if(nodeMap.getNodeValueForNodeCoordinates(nodeXCoord-2,nodeYCoord) !== null) {
            adjacentNodes[adjacentNodes.length] = new Vector(nodeXCoord,nodeYCoord+2);
        }
    }
    
    else if(nodeMap.getNodeValueForNodeCoordinates(nodeXCoord-1, nodeYCoord) !== null)
        adjacentNodes[adjacentNodes.length] = new Vector(nodeXCoord-1,nodeYCoord);
    
    
    
    //Check E
    if(nodeMap.getNodeValueForNodeCoordinates(nodeXCoord+1, nodeYCoord) === 0)
    {
        if(nodeMap.getNodeValueForNodeCoordinates(nodeXCoord+2,nodeYCoord) !== null) {
            adjacentNodes[adjacentNodes.length] = new Vector(nodeXCoord+2,nodeYCoord);
        }
    }
    
    else if(nodeMap.getNodeValueForNodeCoordinates(nodeXCoord+1, nodeYCoord) !== null)
        adjacentNodes[adjacentNodes.length] = new Vector(nodeXCoord+1,nodeYCoord);
   
    return adjacentNodes;
}



/**
 * This heuristic uses a straight line as an estimate to the goal.
 * It is admissible, meaning that the actual path will never be shorter than
 * the straight line distance.  In other words, the heuristic does *not* 
 * overestimate the distance to target.
 * @param {Vector} startPosition the start position for calculation 
 * @param {Vector} endPosition the end position for calculation
 */
function heuristic(startPosition,endPosition) {
    return distanceToPoint(startPosition.x, startPosition.y, endPosition.x,  endPosition.y);
}




/**
 * Run the graph search algorithm A*, which will return the shortest
 * path to the goal (our heuristic is admissible).
 * @param {Vector} startVectorPosition a starting position in the physical game world
 * @param {Vector} goalVectorPosition a goal position in the physical game world
 * @param {NodeMap} the NodeMap that encodes the inner AI representation of the physical game world
 * @return the path, in terms of nodes that go from start to goal
 */
function go(startVectorPosition, goalVectorPosition, nodeMap)
{
    var start = new Path([nodeMap.getNodeCoordinateForPosition(startVectorPosition)]);
    var goal  = new Path([nodeMap.getNodeCoordinateForPosition(goalVectorPosition)]);
    
        // startNode.h = 
        start.h = heuristic(start.pathEnd(),goal.pathEnd());
        start.g = 1;
    
    var closedList = new Array();
        //list of nodes I have explored - don't want to explore again
    
    var openList = new Array();
        openList.push(start);
        //list of candidate nodes to visit; descending sort
        
    var found = null; 
        //when we have found a path from start to goal, put it here.
    
    while (openList.length > 0)
    {
        var current = openList.pop();
            //remove the smallest scoring node from the open list - remember that it's a vector
        
        if(current.pathEnd().x == goal.pathEnd().x && current.pathEnd().y == goal.pathEnd().y) 
        {   //you found a path to the goal. Stop looking!
            found = current;
            break;
        }
        
        closedList[closedList.length] = current;
        var expansion = adjacency(current.pathEnd(), nodeMap);

        for(var e in expansion)
        {
            var expandedPath = new Path(_.flatten([current.path, expansion[e]]));
            expandedPath.h = heuristic(expandedPath.pathEnd(), goal.pathEnd());
            expandedPath.g = current.g + 1;
                //due to the tile-base, it makes sense for the path cost to increase by 1
                
            var c = inPathList(expandedPath, closedList);
            if(c != null) {
//                 
                // //shouldn't happen, since level is monotonic
                // if(expandedPath.g < c.g) 
                // {
                    // for(var i = 0; i < closedList.length; i++) {
                        // if(closedList[i].g === c.g) {
                            // closedList.splice(i,1);
                            // break;
                        // }
                    // }
                // }
            }
            
            var o = inPathList(expandedPath, openList);
            if (o == null) {
                openList[openList.length] = expandedPath;
            }
            
            else 
            {
                // //shouldn't happen, but not mathematically proven
                // //can happen for arbitrary graphs
                // if(expandedPath.g < o.g) 
                // {
                    // //remove the o
                    // for(var j = 0; j < openList.length; j++)  {
                        // if(openList[j].g === o.g) {
                            // openList.splice(j,1);
                            // break;
                        // }
                    // }
//                     
                    // //add the expanded path
                    // openList[openList.length] = expandedPath;
                // }
            }            
        }
        openList.sort();
        openList.reverse();
    }
    return found;    
}