//--------------------------------------------------------------------------
//------------- Player Controller ---------------------------------------------
//--------------------------------------------------------------------------
function PlayerController(_screen) // Default controller
{	BotController.call(this,_screen);
	//alert("Player Controller");
	
	//--------- New Constants-------------------------
	this.botModeActive = true;
	this.indoctrinated = false;
	
	this.indoctroJuice = 0;
	this.indoctroJuiceMAX = 1000;
	//------------------------------------------------
	
	//--------- MAIN UPDATE FUNCTION
	//<<< OVERIDE >>>
	this.update = function()
	{
		this.updateController();//super class update. Very Hacky no super modifier in Javascript
		
		if (this.botModeActive == true)
		{
			this.updateBotController();
			this.UpdateIndoctrinationState();
			
			
		}
		else
		{
			this.updatePlayerController();
			
		}
	
		
	}
	this.updatePlayerController = function()
	{
		this.updatePlayerInput();
		this.getPlayerVector();
		
	}
	
	//------------- Looks at the player input class in _Main_ and determines if the player is moving and rotates the player to face the mouse position.
	this.updatePlayerInput = function()
	{
		
		this.character.rot = rotateToPoint(this.character.pos.x, this.character.pos.y, playerInput.mouseX,  playerInput.mouseY);
		
		if(playerInput.rightKey || playerInput.leftKey || playerInput.upKey || playerInput.downKey)
		{
			this.character.isMoving = true;
			this.character.movingAnim(true);
//			if(this.character.animation.row != 2)
//				this.character.animation.changeRow(2, 8);
		}
		else
		{
			this.character.isMoving = false;
			this.character.movingAnim(false);
//			if(this.character.animation.row != 1)
//				this.character.animation.changeRow(1, 3);
		}
	}
	
	//------------- Figures out the players Vector Angle based on the combination of input keys pressed. 
	this.getPlayerVector = function()
	{
		this.vectorAngleGoal = this.character.rot;
		
		if (playerInput.upKey)
		{
  			//this.vectorAngleGoal = Math.PI/2;
		}
  		else if (playerInput.downKey)
		{
  			this.vectorAngleGoal += Math.PI;
		}
		else if (playerInput.rightKey)
		{
			this.vectorAngleGoal -= Math.PI/2;
		}
  		else if (playerInput.leftKey) 
  		{
  			this.vectorAngleGoal += Math.PI/2;
		}
  		
		/*
		else if (playerInput.rightKey && playerInput.upKey) 
  		{
			this.vectorAngleGoal -= Math.PI/4;
		}
		else if (playerInput.rightKey && playerInput.downKey) 
  		{
			this.vectorAngleGoal -= 3*Math.PI/4;
		}
		else if (playerInput.leftKey && playerInput.upKey) 
  		{
			this.vectorAngleGoal += Math.PI/4;
		}
		else if (playerInput.leftKey && playerInput.downKey) 
  		{
			this.vectorAngleGoal += 3*Math.PI/4;
		}
		*/
		
	}
	this.Indoctrinate = function()
	{

		this.indoctroJuice += 50;
		
		if(this.indoctroJuice >= this.indoctroJuiceMAX)
		{
			this.indoctrinated = true;
			
			this.character.material.map = activeScreen.level.peasentInd.map;
			this.character.map = this.character.material.map;
			
			this.animation = new TextureAnimator(this.character.map, 8, 2, 3, 1, 150 );
			this.character.setAnimated(this.animation);
		}
		
	}
	
	
	this.UnIndoctrinate = function()
	{
		this.indoctrinated = false;
		this.indoctroJuice = 0;
		
		this.character.material.map = activeScreen.level.peasent1.map;
		this.character.map = this.character.material.map;
		
		this.animation = new TextureAnimator(this.character.map, 8, 4, 3, 1, 150 );
		this.character.setAnimated(this.animation);

	}
	
	//---- updates indocrination Level
	this.UpdateIndoctrinationState = function()
	{
		this.indoctroJuice -= 1;
		if(this.indoctroJuice < 0 && this.indoctrinated)
		{
			this.UnIndoctrinate();
		}
	}
	
	this.castIndoctrinate = function()
	{
		
		for (j in _screen.level.characterArray) 
			{
				c =  _screen.level.characterArray [j];
				if( this.character.ID != c.ID  && c.type == 2)
				{
					if(c.checkPointInCollisionBox( new Vector(playerInput.mouseX, playerInput.mouseY) ) && c.myController.indoctrinated == true)
					{
						_screen.takeControlOfCharacter(c);	
					}	
					else if(distanceToPoint(this.character.pos.x,this.character.pos.y, c.pos.x, c.pos.y) < _screen.DoomRadius)
					{
						//console.log("poxy indoct " + distanceToPoint(this.character.pos.x,this.character.pos.y, c.pos.x, c.pos.y) );
						
						c.myController.Indoctrinate();
						
						
					} 
				}
				
				
						
			}
	}
}