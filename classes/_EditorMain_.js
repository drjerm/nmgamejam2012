//--------------ENGINE----------------------------
var container;
var context;
var canvas; 

var DEBUG = true; // Debug mode (show FPS)


var imgArray = new Array(); // should go into a content object

var playerInput; //Contains the Player input class

var activeScreen;

var gameScreen;
var mainMenuScreen;

//------- Initialize
function init() {
	context = $('#canvas')[0].getContext('2d');
	canvas = $('#canvas')[0];
	WIDTH = $('#canvas').width();
	HEIGHT = $('#canvas').height();
	
	playerInput = new Input(); //Contains the Player input class
	
	//Load content. Should go into a content loader
	imgArray[0] = new Image();
	imgArray[0].src = "images/img_null.png";
	imgArray[1] = new Image();
	imgArray[1].src = "images/img_player1.png";
	imgArray[2] = new Image();
	imgArray[2].src = "images/img_player2.png";
	imgArray[3] = new Image();
	imgArray[3].src = "images/img_zombie.png";
	
	//initialize game screens. 
	gameScreen = new GameScreen(WIDTH,HEIGHT);
	gameScreen.Setup();
	activeScreen = gameScreen;
	
    window.requestAnimFrame = (function(callback){
        return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function(callback){
            window.setTimeout(callback, 1000 / 60);
        };
    })();
	
    
    if(DEBUG){
    	container = document.getElementById( 'container' );
    	stats = new Stats();
    	stats.domElement.style.position = 'absolute';
    	stats.domElement.style.top = '0px';
    	//container.appendChild( stats.domElement );
    }
    
	UpdateGame();
	
	
	
	
}


// -------- MAIN GAME LOOP
function UpdateGame() 
{

    
	requestAnimFrame(UpdateGame);

	activeScreen.Update();
	
	if(DEBUG){
		stats.update();
	}

}




