//--------------------------------------------------------------------------
//------------- Input ------------------------------------------------------
//--------------------------------------------------------------------------

function Input() // input for PC, Abstract out later. 
{
	this.projection = new THREE.Projector();
	this.mouse2D = new THREE.Vector3(0, 10000, 0.5);
	this.mousePosition = new THREE.Vector3();
	this.tmpVec = new THREE.Vector3();
	
	
	
	this.screenOffsetX = container.offsetLeft;
	this.screenOffsetY = container.offsetTop;
	

	this.spriteFocused = null;
	
	
	
	this.mouseX   = 0;
	this.mouseY   = 0;
	
	this.mouseLeft = false;
	this.mouseRight = false;
	this.upKey    = false;
    this.downKey  = false;
	this.rightKey = false;
	this.leftKey  = false;
	this.aKey     = false;
	this.sKey     = false;
	this.fKey     = false;
	this.wKey     = false;
	this.xKey     = false;
	
	
	//----- Camera Shit. Should not be here but updates to fucking fast for the CamController to pull it. 
	this.zoomSpeed = 30;
	this.maxZoom = 200;
	this.minZoom = 999;
	this.camZ = 600;
	
	this.updateCamZ = function(delta)
	{
		this.camZ += delta*this.zoomSpeed;
		
		if(this.camZ > this.minZoom)
		{
			this.camZ = this.minZoom;
		}
		if(this.camZ < this.maxZoom)
		{
			this.camZ = this.maxZoom;
		}
		
		
	}
	//----------------
	
	// -------------------------
	this.onKeyDown = function(evt) 
	{

		if (evt.keyCode == 39 || evt.keyCode == 68)
			this.rightKey = true;
		
		else if (evt.keyCode == 37 || evt.keyCode == 65)
			this.leftKey = true;
		
		if (evt.keyCode == 38 || evt.keyCode == 87)
			this.upKey = true;
		
		else if (evt.keyCode == 40 || evt.keyCode == 83)
			this.downKey = true;
			

	    if(evt.keyCode == 88)
            this.xKey = true;

        if(evt.keyCode == 49)
            this.sKey = true;
        
        if(evt.keyCode == 50)
            this.fKey = true;
            
        if(evt.keyCode == 51)
            this.wKey = true;
           
        if(evt.keyCode == 52)
            this.aKey = true;
            
        if(evt.keyCode == 53)
            this.qKey = true;



	}
	// -------------------------
	this.onKeyUp = function(evt) 
	{
		if (evt.keyCode == 39 || evt.keyCode == 68)
			this.rightKey = false;
		
		else if (evt.keyCode == 37 || evt.keyCode == 65)
			this.leftKey = false;
		
		if (evt.keyCode == 38 || evt.keyCode == 87)
			this.upKey = false;
		
		else if (evt.keyCode == 40 || evt.keyCode == 83)
			this.downKey = false;
			

	    if(evt.keyCode == 88)
			  this.xKey = false;

        if(evt.keyCode == 49)
            this.sKey = false;
        
        if(evt.keyCode == 50)
            this.fKey = false;
            
        if(evt.keyCode == 51)
            this.wKey = false;
           
        if(evt.keyCode == 52)
            this.aKey = false;
            
        if(evt.keyCode == 53)
            this.qKey = true;


	}
	
	this.Update = function()
	{
		ray = this.projection.pickingRay(this.mouse2D.clone(), activeScreen.activeCamera);
		
		var intersects = ray.intersectObjects(activeScreen.activeScene.children);

		if (intersects.length > 0) {
			this.setMousePosition(intersects[0]);
			this.spriteFocused = intersects[0].object.sprite;
		} 

	}
	
	 this.setMousePosition = function( intersector ) {
		 this.tmpVec.copy( intersector.face.normal );
		 this.mousePosition.add( intersector.point, intersector.object.matrixRotationWorld.multiplyVector3( this.tmpVec ) );
		 this.mouseX = this.mousePosition.x;
		 this.mouseY = this.mousePosition.y;
		 } 
	
}

// Event listeners. We must find a way to use a local context rather than
// playerInput from _Main_
$(document).mousemove(function(e) {
	//playerInput.mouse2D.x = e.pageX - playerInput.screenOffsetX;
	//playerInput.mouse2D.y = e.pageY - playerInput.screenOffsetY;
	
	playerInput.mouse2D.x = ( e.clientX / window.innerWidth ) * 2 - 1;
	playerInput.mouse2D.y = - ( e.clientY / window.innerHeight ) * 2 + 1; 
});

$(document).mouseup(function(e) {
    if(e.which == 1)playerInput.mouseLeft = false;
	else if(e.which == 3) playerInput.mouseRight = false;
});

	
$(document).mousedown(function(e) {
    
	if(e.which == 1)playerInput.mouseLeft = true;
	else if(e.which == 3) playerInput.mouseRight = true;
	
});

$(document).mousewheel(function(event, delta) {
    playerInput.updateCamZ(delta);
});

$(document).keydown(function(e) {
	playerInput.onKeyDown(e);
}); 

$(document).keyup(function(e) {
	playerInput.onKeyUp(e);
}); 


