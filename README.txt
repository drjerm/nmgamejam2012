# This is the git

Engine TODO

-Switch to vertex objects for all vectors and positions to make function hand offs easier
-radial collision with bounce
-radial vs box collision

-break out AI classes

-write camera, camera translation functions and movement controls/physics (Zoom)? 
-implement basic grid for level with scalable grid

-2D GPU drawing

-2 player replication

-Animation playing and triggering
-Animation loading
-Spritesheets

-Particle Emitters 
-Particle physics (colliding off of other objects?)
-Wind?

-force feedback on collided notstatic objects?
-rotatable rectangular collision possibly with torque?

-Level and menu classes
-Game Conditions

------------- Game mechanics
Enemy spawning
Damage and health/armor
Death and despawn
Bullet spawning 
Ammo?
Weapons?
Inventory?
Pickups?
Random Level generation?



