// JavaScript Document

var curTerrain = 0;
var curEntity = 0;
var curTool = 0;

function OnButtonChanged()
{
  activeScreen.paused = !activeScreen.paused;
  EDITOR = !EDITOR;
}

function OnTerrainSelected(num)
{
	curTerrain = num;
	popup('popUpDiv');
    //alert(num);
    return true;
}


//Handles in-engine controls (CAMERA, CLICKING)
var click = false;
var rclick = false;
var mouseEnabled = true;

function mouseTog(){
//mouseEnabled = !mouseEnabled;
}

var gridMode = true;
function toggleGridMode()
{
gridMode = !gridMode;
}

function createBuilding(){

	var b = null;
		lX = GSC(playerInput.mouseX);
		lY = GSC(playerInput.mouseY);
					
	b = new Building(gameScreen,
		new Vector(lX+gameScreen.level.MasterArray[curEntity].width/2,
					lY-gameScreen.level.MasterArray[curEntity].height/2),
					0, 
					gameScreen.level.MasterArray[curEntity]);

					
	if(b!= null) gameScreen.level.spriteArray[gameScreen.level.spriteArray.length]= b;
}

function GSC(coord)
{
 return coord-(coord%16);
}

function EditorUpdate(){
  
  document.getElementById('InfoPanel').value = "CurTool: " + curTool + " MouseEnable: " + mouseEnabled + "  , |X: " + playerInput.mouseX + ",Y: " + playerInput.mouseY + "|   " + playerInput.spriteFocused;

  if(playerInput.xKey) mouseEnabled = !mouseEnabled;
 
  
 
  if(playerInput.mouseLeft)
  {  if(curTool != 1) {
	click = true;
	return;
	}
	if(playerInput.spriteFocused == undefined) return;
	if(playerInput.spriteFocused.pos == undefined) return;
	playerInput.spriteFocused.pos.x = GSC(playerInput.mouseX);
	playerInput.spriteFocused.pos.y = GSC(playerInput.mouseY);
	playerInput.spriteFocused.UpdateCollisionBox();
	
  }
  if(!playerInput.mouseLeft && click)
  {
   click = !click;
   if(mouseEnabled)
   {    //Put left click code here
   
        switch(curTool)
		{
			case 0:
			if(curEntity < 30) createBuilding();
			else{
			
			}
			
			break;
			case 2:
			//rotate
			if(playerInput.spriteFocused){
			playerInput.spriteFocused.rot += Math.PI/2;
		
			}
			break;
			case 3:
			//delete
			if(playerInput.spriteFocused){
			alert(playerInput.spriteFocused.ID);
			delete gameScreen.level.spriteArray[playerInput.spriteFocused.ID-1];
			}
			break;
			
		}
		
		
	}
  }
}

function OnEntityChanged(dropdown)
{
	curEntity = dropdown.selectedIndex;
    //alert(curEntity);
    return true;
}

function OnToolSelected(dropdown)
{
curTool = dropdown.selectedIndex;
    return true;
}

function SaveLevel()
{
	var printr = "this.spriteArray[this.spriteArray.length]=new Building(_screen,new Vector(" + gameScreen.level.spriteArray[gameScreen.level.spriteArray.length-1].pos.x + "," + gameScreen.level.spriteArray[gameScreen.level.spriteArray.length-1].pos.y + ")," + gameScreen.level.spriteArray[gameScreen.level.spriteArray.length-1].rot + ", this.MasterArray[" + curEntity + "]);";
	alert(printr);
}
